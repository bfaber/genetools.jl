function GeneTools.plot_diagnostic(diag::GeneTools.DiagControl,
                                   ball_diag::GeneTools.Ballooning{T, S, F},
                                   spec::Symbol,
                                   fid;
                                  ) where {T, S, F}
    
    z = fid["z"][:]
    var_list = keys(ball_diag.plot_data)
    n_figures = length(var_list)
    data = Array{Complex{T}}(undef,n_figures,length(z))    

    for v in eachindex(var_list)
        var = var_list[v]
        string_var = String(var)
        data[v,:] = fid[string_var][:]
    end
   
    for f in 1:n_figures
        figure = GeneTools.setup_figure()

        gl = figure[1,1] = GridLayout()
        
        plot_label = plot_moment_labels[ball_diag.var_list[f]]
        real_label = LaTeXString("Re(\$ $(plot_label)\$)")
        imag_label = LaTeXString("Im(\$ $(plot_label)\$)")
        abs_label = LaTeXString("\$ |$(plot_label)|\$")

        ax_lin = Axis(gl[1, 1], 
                      ylabel = LaTeXString("\$ $(plot_label)\$"), 
                      xlabel = L"z/\pi",
                      xminorticksvisible = true,
                      yminorticksvisible = true)
        xlims!(ax_lin,minimum(z),maximum(z))
        ax_log = Axis(gl[2, 1], 
                    ylabel = LaTeXString("\$ $(plot_label)\$"), 
                    xlabel = L"z/\pi",
                    xminorticksvisible = true,
                    yminorticksvisible = true,
                    yminorticks = IntervalsBetween(9),
                    yscale = log10)
        xlims!(ax_log,minimum(z),maximum(z))

        lin_ball_imag = lines!(ax_lin,z,imag.(data[f,:]),
                               label = imag_label,
                               color=Makie.wong_colors()[2],
                               linewidth=2)
        lin_ball_real = lines!(ax_lin,z,real.(data[f,:]),
                               label = real_label,
                               color=Makie.wong_colors()[1],
                               linewidth=2)
        lin_ball_abs = lines!(ax_lin,z,abs.(data[f,:]),
                              label = abs_label,
                              color=:black,
                              linewidth=2)
        
        log_ball_imag = lines!(ax_log,z,abs.(imag.(data[f,:])),
                               label = imag_label,
                               color=Makie.wong_colors()[2],
                               linewidth=2)
        log_ball_real = lines!(ax_log,z,abs.(real.(data[f,:])),
                               label = real_label,
                               color=Makie.wong_colors()[1],
                               linewidth=2)                                              
        log_ball_abs = lines!(ax_log,z,abs.(data[f,:]),
                              label = abs_label,
                              color=:black,
                              linewidth=2)                      
        axislegend(ax_lin, position = :lt)
        axislegend(ax_log, position = :lt)
        
        
        save(diag.out_path * "ballooning_" * String(spec) * "_$(String(var_list[f]))_$(diag.run_IDs[1]).png",figure)
    end
end

function GeneTools.plot_scan_diagnostic(diag::GeneTools.DiagControl,
                                        ball_diag::GeneTools.Ballooning{T, S, F},
                                        spec::Symbol,
                                        fid;
                                       ) where {T, S, F}
    var_list = keys(ball_diag.plot_data)
    n_figures = length(var_list)
    for run in diag.run_IDs
        z = fid["$(run)/z"][:]
        data = Array{Complex{T}}(undef,n_figures,length(z))    

        for v in eachindex(var_list)
            var = var_list[v]
            string_var = String(var)
            data[v,:] = fid["$(run)/$(string_var)"][:]
        end
    
        for f in 1:n_figures
            figure = GeneTools.setup_figure()

            gl = figure[1,1] = GridLayout()
            
            plot_label = plot_moment_labels[ball_diag.var_list[f]]
            real_label = LaTeXString("Re(\$ $(plot_label)\$)")
            imag_label = LaTeXString("Im(\$ $(plot_label)\$)")
            abs_label = LaTeXString("\$ |$(plot_label)|\$")

            ax_lin = Axis(gl[1, 1], 
                        ylabel = LaTeXString("\$ $(plot_label)\$"), 
                        xlabel = L"z/\pi",
                        xminorticksvisible = true,
                        yminorticksvisible = true)
            xlims!(ax_lin,minimum(z),maximum(z))
            ax_log = Axis(gl[2, 1], 
                        ylabel = LaTeXString("\$ $(plot_label)\$"), 
                        xlabel = L"z/\pi",
                        xminorticksvisible = true,
                        yminorticksvisible = true,
                        yminorticks = IntervalsBetween(9),
                        yscale = log10)
            xlims!(ax_log,minimum(z),maximum(z))

            lin_ball_imag = lines!(ax_lin,z,imag.(data[f,:]),
                                label = imag_label,
                                color=Makie.wong_colors()[2],
                                linewidth=2)
            lin_ball_real = lines!(ax_lin,z,real.(data[f,:]),
                                label = real_label,
                                color=Makie.wong_colors()[1],
                                linewidth=2)
            lin_ball_abs = lines!(ax_lin,z,abs.(data[f,:]),
                                label = abs_label,
                                color=:black,
                                linewidth=2)
            
            log_ball_imag = lines!(ax_log,z,abs.(imag.(data[f,:])),
                                label = imag_label,
                                color=Makie.wong_colors()[2],
                                linewidth=2)
            log_ball_real = lines!(ax_log,z,abs.(real.(data[f,:])),
                                label = real_label,
                                color=Makie.wong_colors()[1],
                                linewidth=2)                                              
            log_ball_abs = lines!(ax_log,z,abs.(data[f,:]),
                                label = abs_label,
                                color=:black,
                                linewidth=2)                      
            axislegend(ax_lin, position = :lt)
            axislegend(ax_log, position = :lt)
            
            
            save(diag.out_path * "ballooning_" * String(spec) * "_$(String(var_list[f]))_$(run).png",figure)
        end
    end
end