function GeneTools.plot_diagnostic(diag::GeneTools.DiagControl,
                                   flux_diag::GeneTools.FluxSpectra{T, S, F},
                                   spec::Symbol,
                                   fid;
                                  ) where {T, S, F}
    kx = fid["kx"][:]
    ky = fid["ky"][:]
    figure = GeneTools.setup_figure()
    gl = figure[1,1] = GridLayout()
    axx = Axis(gl[1, 1], 
                ylabel = LaTeXString("Flux \$(c_\\textrm{s}n_\\textrm{ref}T_\\textrm{ref}(\\rho_\\textrm{s}/L_\\textrm{ref})^2)\$"), 
                xlabel = LaTeXString("\$$(plot_resolution_labels[:kx])\$"),
                xminorticksvisible = true,
                yminorticksvisible = true)
    axy = Axis(gl[1, 2], 
                ylabel = LaTeXString("Flux \$(c_\\textrm{s}n_\\textrm{ref}T_\\textrm{ref}(\\rho_\\textrm{s}/L_\\textrm{ref})^2)\$"), 
                xlabel = LaTeXString("\$ $(plot_resolution_labels[:ky])\$"),
                xminorticksvisible = true,
                yminorticksvisible = true)
    fid_keys = filter(x->x=="kx"||x=="ky",keys(fid))
    for f in eachindex(flux_diag.flux_list)
        flux = flux_diag.flux_list[f]
        fid_flux_kx = String(flux) * ", kx"
        fid_flux_ky = String(flux) * ", ky"
        lines!(axx,kx,fid[fid_flux_kx][:],
               label = LaTeXString("\$$(plot_flux_labels[flux])\$"),
               linewidth = 2)
        lines!(axy,ky,fid[fid_flux_ky][:],
               label = LaTeXString("\$$(plot_flux_labels[flux])\$"),
               linewidth = 2)
    end
    xlims!(axx,0,nothing)
    xlims!(axy,0,nothing)
    ylims!(axx,0,nothing)
    ylims!(axy,0,nothing)
    axislegend(axx, position = :rt)
    axislegend(axy, position = :rt)
        
        
    save(diag.out_path * "fluxspectra_" * String(spec) * "_$(diag.run_IDs[1])_$(diag.run_IDs[end]).png",figure)
   
end

#One flux at a time, kx, or ky. Assumes minor radius as normalizing length for visualization
function GeneTools.compare_flux_spectra(file_list::Vector{String};
		scale_factors::Union{Vector{Float64},Vector{Int}} = ones(Int,length(file_list)),
                                        flux::Symbol = :Q_es,
                                        wavenumber::Symbol = :ky,
					line_styles::Vector{Symbol} = fill(:solid,length(file_list)),
					plot_labels::Union{Vector{String},Vector{AbstractString},Vector{LaTeXString}} = [],
                                        out_path::String = ".",
                                        kwargs...
                                        ) 
    n_files = length(file_list)
    flux_string = String(flux) * ", " * String(wavenumber)
    k_string = String(wavenumber)
    figure = GeneTools.setup_figure()
    flux_label = LaTeXString("\$$(plot_flux_labels[flux])\\quad c_\\textrm{s}n_\\textrm{ref}T_\\textrm{ref}(\\rho_\\textrm{s}/a^2)\$")
    k_label = LaTeXString("\$$(plot_resolution_labels[wavenumber])")
    if plot_labels == []
        plot_labels = copy(file_list)
    end
    figure = GeneTools.setup_figure(fontsize=64)
    gl = figure[1,1] = GridLayout()
    ax = Axis(gl[1, 1]; 
                ylabel = flux_label, 
                xlabel = k_label,
                kwargs...)
    for f in eachindex(file_list)
        fid = h5open(file_list[f],"r")
	flux_data = try
	    fid[flux_string][:]*scale_factors[f]
	catch KeyError
	    fid[String(flux) * ",p," * String(wavenumber)][:]*scale_factors[f]
	end

        lines!(ax,fid[k_string][:],flux_data,
               	   label = plot_labels[f],
                   linewidth = 4,
                   linestyle = line_styles[f]
                  )
        close(fid)
    end

    axislegend(ax, position = :rt)
    out_name = out_path * "fluxspectra"

    out_name = out_name * "_" * String(flux) * "_" * k_string
    save(out_name * ".png",figure)

end
