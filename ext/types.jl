
plot_moment_labels = Dict(
    :dens => "n",
    :T_par => "T_\\parallel",
    :T_perp => "T_\\perp",
    :q_par => "q_\\parallel",
    :q_perp => "q_\\perp",
    :u_par => "u_\\parallel",
    :dens_pass => "n_p",
    :T_par_pass => "T_{\\parallel,p}",
    :T_perp_pass => "T_{\\perp,p}",
    :q_par_pass => "q_{\\parallel,p}",
    :q_perp_pass => "q_{\\perp,p}",
    :u_par_pass => "u_{\\parallel,p}",
    :dens_trap => "n_t",
    :T_par_trap => "T_{\\parallel,t}",
    :T_perp_trap => "T_{\\perp,t}",
    :q_par_trap => "q_{\\parallel,t}",
    :q_perp_trap => "q_{\\perp,t}",
    :u_par_trap => "u_{\\parallel,t}",
    :dens_flr => "n_\\textrm{flr}",
    :T_par_flr => "T_{\\parallel,\\textrm{flr}}",
    :T_perp_flr => "T_{\\perp,\\textrm{flr}}",
    :q_par_flr => "q_{\\parallel,\\textrm{flr}}",
    :q_perp_flr => "q_{\\perp,\\textrm{flr}}",
    :u_par_flr => "u_{\\parallel,\\textrm{flr}}",
    :phi => "\\phi",
    :A_par => "A_\\parallel",
    :B_par => "B_\\parallel"
)

plot_resolution_labels = Dict(
    :kymin => "k_y\\rho_{\\textrm{s}}",
    :ky => "k_y\\rho_{\\textrm{s}}",
    :kx_center => "k_x\\rho_{\\textrm{s}}",
    :kx => "k_x\\rho_{\\textrm{s}}"
)

plot_eigenvalue_labels = Dict(
    :gamma => "\\gamma\\quad(a/c_\\text{s})",
    :omega => "\\omega\\quad(a/c_\\text{s})"
)

plot_geometry_labels = Dict(
    :gxx => "g^{xx}",
    :gxy => "g^{xy}",
    :gyy => "g^{yy}",
    :jac => "\\sqrt{g}",
    :modB => "B",
    :K2 => "K^y",
    :K1 => "K^x",
    :dBdz => "\\partial B/\\partial z"
)

plot_flux_labels = Dict(
    :Γ_es => "\\Gamma_{\\textrm{es}}",
    :Q_es => "Q_{\\textrm{es}}",
    :Γ_em => "\\Gamma_{\\textrm{em}}",
    :Q_em => "Q_{\\textrm{em}}"
)

plot_plasma_labels = Dict(
    :beta => "\\beta"
)

plot_labels = merge(plot_moment_labels,plot_resolution_labels,plot_eigenvalue_labels,plot_geometry_labels,plot_flux_labels,plot_plasma_labels)
