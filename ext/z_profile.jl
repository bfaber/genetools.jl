function GeneTools.plot_diagnostic(diag::GeneTools.DiagControl,
                                   z_prof_diag::GeneTools.ZProfile{T, S, F},
                                   spec::Symbol,
                                   fid;
                                  ) where {T, S, F}
    
    
    var_list = keys(z_prof_diag.plot_data)
    z = fid["z"][:]
    kx = fid["kx"][:]
    ky = fid["ky"][:]
    nkx = length(kx)
    nky = length(ky)
    n_figures = length(var_list)*nkx*nky
    n_vars = length(var_list)
    data = Array{Complex{T}}(undef,n_vars,nkx,nky,length(z))    

    for v in eachindex(var_list)
        var = var_list[v]
        string_var = String(var)
        data[v,:,:,:] = fid[string_var][:,:,:]
    end
   
    for f in 1:n_figures
        figure = GeneTools.setup_figure()

        gl = figure[1,1] = GridLayout()
        v = div(f,nkx*nky)
        x = div(f - (v-1)*nkx*nky,nky) 
        y = Int((f - (v-1)*nkx*nky) % nky) + 1
        
        plot_label = plot_moment_labels[z_prof_diag.var_list[v]]
        
        abs_label = LaTeXString("\$ |$(plot_label)|\$")

        ax = Axis(gl[1, 1], 
                      ylabel = abs_label, 
                      xlabel = L"z/\pi",
                      xminorticksvisible = true,
                      yminorticksvisible = true)
        xlims!(ax,minimum(z),-minimum(z))
        ylims!(ax,0,nothing)
       
        profile = lines!(ax,z,abs.(data[v,x,y,:]),
                              label = abs_label,
                              color=:black,
                              linewidth=2)
        
        save(diag.out_path * "z_profile_" * String(spec) * "_$(String(var_list[v]))_$(kx[x])_$(ky[y])_$(diag.run_IDs[1]).png",figure)
    end
end

function GeneTools.plot_scan_diagnostic(diag::GeneTools.DiagControl,
                                        z_prof_diag::GeneTools.ZProfile{T, S, F},
                                        spec::Symbol,
                                        fid;
                                       ) where {T, S, F}
    var_list = keys(z_prof_diag.plot_data)
    for run in diag.run_IDs
        z = fid["$(run)/z"][:]
        kx = fid["$(run)/kx"][:]
        ky = fid["$(run)/ky"][:]
        nkx = length(kx)
        nky = length(ky)
        var_list = keys(z_prof_diag.plot_data)
        n_figures = length(var_list)*nkx*nky
        n_vars = length(var_list)
        data = Array{T}(undef,n_vars,nkx,nky,length(z))    

        for v in eachindex(var_list)
            var = var_list[v]
            string_var = String(var)
            data[v,:,:,:] = fid["$(run)/$(string_var)"][:,:,:]
        end
    
        for f in 1:n_figures
            figure = GeneTools.setup_figure()

            gl = figure[1,1] = GridLayout()
            v = div(f,nkx*nky)
            x = div(f - (v-1)*nkx*nky,nky)
            y = Int((f - (v-1)*nkx*nky) % nky) + 1
            
            plot_label = plot_moment_labels[z_prof_diag.var_list[v]]
            
            abs_label = LaTeXString("\$ |$(plot_label)|\$")

            ax = Axis(gl[1, 1], 
                        ylabel = abs_label, 
                        xlabel = L"z/\pi",
                        xminorticksvisible = true,
                        yminorticksvisible = true)
            xlims!(ax,minimum(z),-minimum(z))
            ylims!(ax,0,nothing)

            profile = lines!(ax,z,abs.(data[v,x,y,:]),
                                label = abs_label,
                                color=:black,
                                linewidth=2)
 
            save(diag.out_path * "z_profile_" * String(spec) * "_$(String(var_list[v]))_$(kx[x])_$(ky[y])_$(run).png",figure)
        end
    end
end


function GeneTools.compare_z_profile(z_profile_files::Vector{String},
                                     profile_list::Vector{Symbol},
				     plot_labels::Vector{AbstractString};
                                     out_path = ".",
                                     kwargs...)
    n_files = length(z_profile_files)
    n_figures = length(profile_list)
    z_p = Vector{Any}()

    for f in 1:n_figures
        figure = GeneTools.setup_figure()

        var = profile_list[f]
        string_var = String(var)
        
	gl = figure[1,1] = GridLayout()
        
	elem = Vector{Any}(undef,n_files)
        legend_labels = Vector{Any}(undef,n_files)
        plot_label = LaTeXString("\$|$(plot_moment_labels[var])|\$")
        ax = Axis(gl[1, 1];
                  ylabel = plot_label,
                  xlabel = L"z/\pi",
                  kwargs...)
        for p in 1:n_files
            file = z_profile_files[p]
	    fid = h5open(file, "r")
            z_p = fid["z"][:]
            float_type = typeof(z_p[1])
            data = fid[string_var][1,1,:]
            
	    line_color = Makie.wong_colors()[p]
            profile = lines!(ax,z_p,data,
                            label = plot_labels[p],
                            color = line_color,
                            linewidth = 4)
	    ylims!(0,nothing)
	    legend_labels[p] = plot_labels[p]
            elem[p] = LineElement(color = line_color, 
                                  linestyle = nothing, 
                                  linewidth = 4,
                                  points = Point2f[(0, 0.5), (1, 0.5)])
	    close(fid)
        end
        axislegend(ax,elem,legend_labels)

        if plot_labels == []
            plot_labels = copy(z_profile_files)
        end
        out_name = out_path * "z_profile"

        save(out_name * String(profile_list[f]) * ".png",figure)
    end
end

function GeneTools.compare_z_profile_and_geometry(geom_file::String,
                                                  geom_type::String,
                                                  geom_list::Union{Symbol, Vector{Symbol}},
						  z_profile_files::Vector{String},
                                                  profile_list::Vector{Symbol},
						  plot_labels::Union{Vector{AbstractString},Vector{String},Vector{LaTeXString}};
						  plot_style::Vector{Symbol} = fill(:solid,length(z_profile_files)),
                                                  out_path = ".",
                                                  kwargs...)
    geom = GeneTools.set_geometry(geom_file,geom_type)
    geom_dict = Dict(:gxx => geom.gxx,
                     :gxy => geom.gxy,
                     :gyy => geom.gyy,
                     :jac => geom.jac,
                     :modB => geom.modB,
                     :K2 => geom.K2,
                     :K1 => geom.K1,
                     :dBdz => geom.dBdz
                     )
    z_g = geom.theta
    n_files = length(z_profile_files)
    n_geoms = length(geom_list)
    n_figures = length(profile_list)
    z_p = Vector{Any}()
    data = Vector{Array}(undef,n_figures)

    for f in 1:n_figures
        figure = GeneTools.setup_figure()

        var = profile_list[f]
        string_var = String(var)
        
	gl = figure[1,1] = GridLayout()
        
	elem = Vector{Any}(undef,n_geoms + n_files)
        legend_labels = Vector{Any}(undef, n_geoms + n_files)
        ax = nothing
	geom_colors = [:black, Makie.wong_colors()[1]]
	geom_styles = [:dash, :dashdot]
        for g in 1:n_geoms
            geom_var = geom_list[g]
            geom_label = LaTeXString("\$$(plot_geometry_labels[geom_var])\$")
            y_ax_pos = g == 1 ? :left : :right
	    line_color = geom_colors[g]#Makie.wong_colors()[g]
            ax = Axis(gl[1, 1];
                          ylabel = geom_label,
                          xlabel = L"z/\pi",
                          yaxisposition = y_ax_pos, 
                          kwargs...)
            ax.xgridvisible = false
            ax.ygridvisible = false
            if geom_var == :gxx || geom_var == :gyy
                ylims!(ax,0,nothing)
            end
            if geom_var == :K2 || geom_var == :K1 || geom_var == :gxy || geom_var == :dBdz
                hlines!(ax,0,color=:black,linewidth=0.75)
            end
            lines!(ax,z_g,geom_dict[geom_var],
                   color = line_color,
                   linewidth = 3,
                   label = geom_label,
		   linestyle = geom_styles[g])
            legend_labels[g] = geom_label
            elem[g] = LineElement(color = line_color, 
				  linestyle = geom_styles[g], 
                                  linewidth = 3,
                                  points = Point2f[(0, 0.5), (1, 0.5)])
        end
        for p in 1:n_files
            file = z_profile_files[p]
	    fid = h5open(file, "r")
            z_p = fid["z"][:]
            float_type = typeof(z_p[1])
            data = fid[string_var][1,1,:]
            
	    line_color = Makie.wong_colors()[p+1] #+ n_geoms]
            profile = lines!(ax,z_p,data,
                            label = plot_labels[p],
                            color = line_color,
                            linewidth = 4,
			    linestyle = plot_style[p])
            ax.xgridvisible = false
            ax.ygridvisible = false   
            legend_labels[p + n_geoms] = plot_labels[p]
            elem[p + n_geoms] = LineElement(color = line_color, 
                                  linestyle = nothing, 
                                  linewidth = 4,
                                  points = Point2f[(0, 0.5), (1, 0.5)])
	    close(fid)
        end
        axislegend(ax,elem,legend_labels)

        if plot_labels == []
            plot_labels = copy(z_profile_files)
        end
        out_name = out_path * "z_profile"
        for g in geom_list
            out_name = out_name * "_" * String(g)
        end

        save(out_name * String(profile_list[f]) * ".png",figure)
    end
end
