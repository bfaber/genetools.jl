#can only handle one scan parameter right now
function GeneTools.plot_scan_diagnostic(diag::GeneTools.DiagControl,
                                        eigenvalues_diag::GeneTools.Eigenvalues{T, S, F},
                                        spec::Symbol,
                                        fid;
                                       ) where {T, S, F}
    var_list = keys(eigenvalues_diag.plot_data)
    scan_pars = collect(keys(diag.scan_parameters))
    scan_vals = diag.scan_parameters[scan_pars[1]]
    data = Array{T}(undef,length(var_list),length(scan_vals))    

    for v in eachindex(var_list)
        var = var_list[v]
        string_var = String(var)
        for r in eachindex(diag.run_IDs)
            run = diag.run_IDs[r]
            data[v,r] = fid["$run/$string_var"][:][1]
        end
    end
    
    figure = GeneTools.setup_figure()

    gl = figure[1,1] = GridLayout()
        

    gamma_label = LaTeXString("\$ $(plot_eigenvalue_labels[eigenvalues_diag.var_list[2]])\$")
    omega_label = LaTeXString("\$ $(plot_eigenvalue_labels[eigenvalues_diag.var_list[3]])\$")

    scan_label = LaTeXString("\$$(plot_resolution_labels[scan_pars[1]])\$")

    ax_gamma = Axis(gl[1, 1], 
                ylabel = gamma_label, 
                xlabel = scan_label,
                xminorticksvisible = true,
		        xticks = ([0.1,1,10],[L"10^{-1}", L"10^{0}", L"10^{1}"]),
		        xminorticks = IntervalsBetween(9),
                xscale = log10)
    ylims!(ax_gamma,0,nothing)
    ax_omega = Axis(gl[2, 1], 
                ylabel = omega_label, 
                xlabel = scan_label,
                xminorticksvisible = true,
                xticks = ([0.1,1,10],[L"10^{-1}", L"10^{0}", L"10^{1}"]),
		        xminorticks = IntervalsBetween(9),
                xscale = log10)
    hlo = hlines!(ax_omega,0,color=:black,linewidth=0.75)

    lg = scatterlines!(ax_gamma,scan_vals,data[2,:],
                marker=:circle,
                markercolor=:transparent,
                markersize=20,
                strokewidth=4,
                strokecolor=Makie.wong_colors()[1],
                linewidth=4)
    lo = scatterlines!(ax_omega,scan_vals,data[3,:],
                marker=:rect,
                markercolor=:transparent,
                strokewidth=4,
                markersize=20,
                strokecolor=Makie.wong_colors()[2],
                color=Makie.wong_colors()[2],
                linewidth=4)
    #axislegend(ax, position = :rt)
    
    save(diag.out_path * "eigenvalues.png",figure)
end

#Assumes kymin is scan variable
function GeneTools.compare_eigenvalues(file_list::Vector{String},
				       scan_var::Symbol;
                                       var_list::Vector{Symbol} = [:gamma, :omega],
				       series_names::Union{Vector{AbstractString},Vector{LaTeXString},Vector{String}} = [],
                                       markers::Vector{Symbol} = [:circle, :rect, :utriangle, :diamond],
				       line_style::Vector{Symbol} = fill(:solid,length(file_list)),
                                       out_path::String = ".",
                                       kwargs...) 
    
    n_vars = length(var_list)
    n_files = length(file_list)
    scan_var_string = String(scan_var)
    fid = h5open(file_list[1], "r")
    fid[scan_var_string][:]
    float_type = typeof(fid[scan_var_string][:][1])
    close(fid)
    file_data = Vector{Array{float_type}}(undef,n_files)

    ky_data = Vector{Array{float_type}}(undef,n_files)

    for f in eachindex(file_list)
        file = file_list[f]
        fid = h5open(file, "r")
        ky_data[f] = fid[scan_var_string][:]
        float_type = typeof(ky_data[f][1])
        n_runs = length(ky_data[f])

        #Get data for each run and each var in var_list
        data = Array{float_type}(undef,n_vars,n_runs)
        run_list = keys(fid)
        filter!(x->x≠scan_var_string,run_list)
        for r in eachindex(run_list)
            run = run_list[r]
            for v in eachindex(var_list)
                string_var = String(var_list[v])
                data[v,r] = fid["$(run)/$(string_var)"][:][1]
            end
        end
        file_data[f] = deepcopy(data)
        close(fid)
    end


    figure = GeneTools.setup_figure(fontsize=48)
    if series_names == []
        series_names = copy(file_list)
    end
    
    scan_label = LaTeXString("\$$(plot_labels[scan_var])\$")
    
    #Make plot_diagnostic
    for v in eachindex(var_list)
        gl = figure[v, 1] = GridLayout()

        var_label = LaTeXString("\$ $(plot_eigenvalue_labels[var_list[v]])\$")
        if v == n_vars
            ax = Axis(gl[1, 1]; 
                        ylabel = var_label, 
                        xlabel = scan_label,
                        kwargs...)
        else
            ax = Axis(gl[1, 1]; 
                      ylabel = var_label, 
                      kwargs...)
        end
	if var_list[v] == :gamma
            ylims!(0,nothing)
	elseif var_list[v] == :omega
    	    hlines!(ax,0,color=:black,linewidth=0.75)
	end
        for f in eachindex(file_list)
	    ev_data = filter(x->x!=0,file_data[f][v,:])
	    ky = ky_data[f][filter(x->file_data[f][v,x]!=0,[1:length(ky_data[f]);])] 
            l = scatterlines!(ax, ky, ev_data,
                          label = series_names[f],
                          marker=markers[f],
                          markercolor=:transparent,
                          markersize=20,
                          strokewidth=4,
                          strokecolor=Makie.wong_colors()[f],
                          linewidth=4,
			  linestyle = line_style[f]
                          )
        end
        if v == 1 
	    axislegend(ax, position = :lt)
        end
    end

    out_name = out_path * "eigenvalues"
    for f in series_names
        out_name = out_name * "_" * f
    end
    for v in var_list
        out_name = out_name * "_" * String(v)
    end

    save(out_name * ".png",figure)

end
