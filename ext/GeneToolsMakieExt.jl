module GeneToolsMakieExt

using GeneTools
using Makie
using MathTeXEngine
using HDF5
using LaTeXStrings

include("types.jl")
include("plots.jl")

include("cross_phases.jl")
include("eigenvalues.jl")
include("ballooning.jl")
include("z_profile.jl")
include("geometry.jl")
include("flux_spectra.jl")

end