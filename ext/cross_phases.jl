# Only good for nky = 1 right now
function GeneTools.plot_diagnostic(diag::GeneTools.DiagControl,
                                   cross_phases_diag::GeneTools.CrossPhases{T, S, F},
                                   spec::Symbol,
                                   fid;
                                  ) where {T, S, F}
    
    phases = fid["phases"][:]
    var_list = keys(cross_phases_diag.plot_data)
    nbins = size(cross_phases_diag.plot_data[var_list[1]])[1] - 2
    n_plots = length(var_list)
    mean = zeros(T,n_plots)
    histogram = zeros(T,nbins+1,n_plots)
    for v in eachindex(var_list)
        var = var_list[v]
        string_var = String(var)
        mean[v] = fid[string_var]["mean"][:][1]
        histogram[:,v] .= fid[string_var]["histogram"][:]
    end
    
    n_figures = Int(ceil(n_plots/3))
    for f in 1:n_figures
        figure = GeneTools.setup_figure()
        if f == n_figures
		n_cols = n_plots < 3 ? n_plots : n_plots-3*(f-1)
        else
            n_cols = 3
        end
        ax = Vector{Any}(undef,n_cols)
        for c in 1:n_cols
            plot_ID = 3*(f-1)+c
	    gl = figure[1,c] = GridLayout()
            phase_label1 = plot_moment_labels[cross_phases_diag.var_list[plot_ID][1]]
            phase_label2 = plot_moment_labels[cross_phases_diag.var_list[plot_ID][2]]
            phase_label = LaTeXString("\$ $(phase_label1)\\times $(phase_label2)\$")
            ax[c] = Axis(gl[1, 1], xticks = ([-π,-π/2,0,π/2,π],[L"-\pi",L"-\pi/2","0",L"\pi/2",L"\pi"]), xlabel = L"\alpha", ylabel=phase_label,limits=(-pi,pi,0,nothing))
            l = lines!(ax[c],phases,histogram[:,plot_ID],linewidth=2)
            vl = vlines!(ax[c],mean[plot_ID],color=:red)
        end
	save(diag.out_path * "cross_phase_" * String(spec) * "_$(Int(f))_$(diag.run_IDs[1]).png",figure)
    end
end

#can only handle one scan parameter right now
function GeneTools.plot_scan_diagnostic(diag::GeneTools.DiagControl,
                                        cross_phases_diag::GeneTools.CrossPhases{T, S, F},
                                        spec::Symbol,
                                        fid;
                                       ) where {T, S, F}
    var_list = keys(cross_phases_diag.plot_data)
    scan_pars = collect(keys(diag.scan_parameters))
    scan_vals = diag.scan_parameters[scan_pars[1]]
    mean = Array{T}(undef,length(var_list),length(scan_vals))
    #pass/trap specific setup
    

    for v in eachindex(var_list)
        var = var_list[v]
        string_var = String(var)
        for r in eachindex(diag.run_IDs)
            run = diag.run_IDs[r]
            mean[v,r] = fid["$run/$string_var"]["mean"][:][1]
        end
    end
    n_figures = Int(ceil(length(var_list)/2))
    for f in 1:n_figures
        figure = GeneTools.setup_figure()

        var_index_1 = 2*(f-1) + 1
        var_index_2 = 2*(f-1) + 2
        gl = figure[1,1] = GridLayout()
            
    
        phase_label11 = plot_moment_labels[cross_phases_diag.var_list[var_index_1][1]]
        phase_label12 = plot_moment_labels[cross_phases_diag.var_list[var_index_1][2]]
        phase_label1 = LaTeXString("\$ $(phase_label11)\\times $(phase_label12)\$")
        phase_label21 = plot_moment_labels[cross_phases_diag.var_list[var_index_2][1]]
        phase_label22 = plot_moment_labels[cross_phases_diag.var_list[var_index_2][2]]
        phase_label2 = LaTeXString("\$ $(phase_label21)\\times $(phase_label22)\$")
        
        scan_label = LaTeXString("\$$(plot_resolution_labels[scan_pars[1]])\$")

        ax = Axis(gl[1, 1], 
                  yticks = ([-π,-π/2,0,π/2,π],[L"-\pi",L"-\pi/2","0",L"\pi/2",L"\pi"]), 
                  ylabel = L"\alpha", 
                  xlabel=scan_label,
                  xscale = log10,
		          xminorticksvisible = true,
		          xticks = ([0.1,1,10],[L"10^{-1}", L"10^{0}", L"10^{1}"]),
		          xminorticks = IntervalsBetween(9),
                  limits=(nothing,nothing,-pi,pi))
        hl = hlines!(ax,0,color=:black,linewidth=0.75)
        l1 = scatterlines!(ax,scan_vals,mean[var_index_1,:],
                    marker=:circle,
                    markercolor=:transparent,
                    markersize=20,
                    strokewidth=4,
                    strokecolor=Makie.wong_colors()[1],
                    linewidth=4,
                    label=phase_label1)
        l2 = scatterlines!(ax,scan_vals,mean[var_index_2,:],
                    marker=:rect,
                    markercolor=:transparent,
                    strokewidth=4,
                    markersize=20,
                    strokecolor=Makie.wong_colors()[2],
                    linewidth=4,
                    label=phase_label2)
        axislegend(ax, position = :lt)
        
        save(diag.out_path * "cross_phase_scan_" * String(spec) * "_$(Int(f)).png",figure)
    end
end

function GeneTools.plot_mean_cross_phases(file::String,
		var_list::Vector{Tuple{Symbol,Symbol}},
                                          vars_per_plot::Int;
					  legend_pos::Vector{Symbol} = [:lb],
                                          markers::Vector{Symbol} = [:circle, :rect, :utriangle, :diamond],
                                          out_path::String = ".",
                                          kwargs...
                                          )
    n_vars = length(var_list)
    fid = h5open(file, "r")
    ky = fid["kymin"][:]
    float_type = typeof(fid["kymin"][:][1])
    run_list = collect(keys(fid))
    filter!(x->x≠"kymin",run_list)
    n_runs = length(run_list)
    mean = Array{float_type}(undef,n_vars,n_runs)
    n_subfigures = Int(ceil(length(var_list)/(vars_per_plot)))
    
    if legend_pos == [:lb]
	legen_pos = fill(:lb,n_subfigures)
    end

    scan_label = LaTeXString("\$$(plot_resolution_labels[:kymin])\$")
    for r in eachindex(run_list)
        run = run_list[r]
        for v in eachindex(var_list)
		string_var = String(var_list[v][1])*"_x_"*String(var_list[v][2])
		mean[v,r] = fid[run][string_var]["mean"][1]
        end
    end
    close(fid)

    figure = GeneTools.setup_figure()

    for f in 1:n_subfigures
        gl = figure[f,1] = GridLayout()
        if f == n_subfigures
            ax = Axis(gl[1, 1];
                    yticks = ([-π,-π/2,0,π/2,π], [L"-\pi",L"-\pi/2","0",L"\pi/2",L"\pi"]), 
                    ylabel = L"\alpha", 
                    xlabel = scan_label,
                    kwargs...)
        else
            ax = Axis(gl[1, 1];
                    yticks = ([-π,-π/2,0,π/2,π], [L"-\pi",L"-\pi/2","0",L"\pi/2",L"\pi"]),
		    xticklabelsvisible=false,
                    ylabel = L"\alpha", 
                    kwargs...)
        end
        ylims!(ax,-pi,pi)
        hlines!(ax,0,color=:black,linewidth=0.75)
           
        for v in 1:vars_per_plot
            var_index = vars_per_plot*(f-1) + v
            phase_label1 = plot_moment_labels[var_list[var_index][1]]
            phase_label2 = plot_moment_labels[var_list[var_index][2]]
            phase_label = LaTeXString("\$ $(phase_label1)\\times $(phase_label2)\$")
                   
            scatterlines!(ax,ky,mean[var_index,:],
                          marker=markers[v],
                          markercolor=:transparent,
                          markersize=20,
                          strokewidth=4,
                          strokecolor=Makie.wong_colors()[v],
                          linewidth=4,
                          label=phase_label)
        end
	axislegend(ax,position=legend_pos[f],labelsize=48-(n_subfigures-1)*6)
    end
    out_name = out_path * "cross_phases"

    for v in var_list
        out_name = out_name * "_" * String(v[1]) * "_x_" * String(v[2])
    end

    save(out_name * ".png",figure)
end


function GeneTools.plot_linear_and_nonlinear_cross_phases(linear_file::String,
							  nonlinear_file::String,
							  var_list::Vector{Tuple{Symbol,Symbol}};
                                               		  out_path::String = ".",
                                          		  kwargs...
                                          		 )
    n_vars = length(var_list)
    fid_lin = h5open(linear_file, "r")
    ky_linear = fid_lin["kymin"][:]
    float_type_lin = typeof(fid_lin["kymin"][:][1])
    run_list = collect(keys(fid_lin))
    filter!(x->x≠"kymin",run_list)
    n_runs = length(run_list)
    mean = Array{float_type_lin}(undef,n_vars,n_runs)

    scan_label = LaTeXString("\$$(plot_resolution_labels[:kymin])\$")
    for r in eachindex(run_list)
        run = run_list[r]
        for v in eachindex(var_list)
		string_var = String(var_list[v][1])*"_x_"*String(var_list[v][2])
		mean[v,r] = fid_lin[run][string_var]["mean"][1]
        end
    end
    close(fid_lin)

    fid_nl = h5open(nonlinear_file, "r")  
    ky_nl = fid_nl["ky"][:]
    phases = fid_nl["phases"][:]
    nbins = length(phases)-1
    hist_nl = Array{float_type_lin}(undef,n_vars,nbins+1,length(ky_nl))
    float_type_nl = typeof(fid_nl["ky"][:][1])
    for v in eachindex(var_list)
	string_var = String(var_list[v][1])*"_x_"*String(var_list[v][2])
    	hist_nl[v,:,:] = fid_nl[string_var]["histogram"][:,:]
    end
    
    close(fid_nl)
    figure = GeneTools.setup_figure()
    for f in 1:length(var_list)
	var_index = f
        phase_label1 = plot_moment_labels[var_list[var_index][1]]
        phase_label2 = plot_moment_labels[var_list[var_index][2]]
        phase_label = LaTeXString("\$ $(phase_label1)\\times $(phase_label2)\$")
	if f == length(var_list)
            ax = Axis(figure[f, 1];
                    yticks = ([-π,-π/2,0,π/2,π], [L"-\pi",L"-\pi/2","0",L"\pi/2",L"\pi"]), 
                    ylabel = phase_label, 
                    xlabel = scan_label,
                    kwargs...)
        else
            ax = Axis(figure[f, 1];
                    yticks = ([-π,-π/2,0,π/2,π], [L"-\pi",L"-\pi/2","0",L"\pi/2",L"\pi"]),
		    xticklabelsvisible=false,
                    ylabel = phase_label, 
                    kwargs...)
        end
        ylims!(ax,-pi,pi)
	contourf!(ky_nl,phases,transpose(log10.(hist_nl[var_index,:,:])),colormap=:jet1,levels=100)
        
        lines!(ax,ky_linear,mean[var_index,:],
                            linewidth=4,
                            label=phase_label,
			    color=:black)
    end
    out_name = out_path * "cross_phases_lin_nl"

    for v in var_list
        out_name = out_name * "_" * String(v[1]) * "_x_" * String(v[2])
    end

    save(out_name * ".png",figure)
end


