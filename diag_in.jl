
start_time = 100.0
end_time = 130.53
last_time_step = true
data_path = "C:/Users/joeyd//Projects/d-25_linear_omn4_omte0/"
run_IDs = "0002"
plot_type = :CairoMakie
active_species = [:ions, :electrons]
diagnostics = [:(CrossPhases([(:phi, :dens_pass), (:phi, :dens_trap)]))]
sparse_step = 1
time_average = true
