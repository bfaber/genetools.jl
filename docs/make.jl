using GeneTools
using Documenter

DocMeta.setdocmeta!(GeneTools, :DocTestSetup, :(using GeneTools); recursive=true)

makedocs(;
    modules=[GeneTools],
    authors="Benjamin Faber <bfaber@wisc.edu> and contributors",
    repo="https://gitlab.com/bfaber/GeneTools.jl/blob/{commit}{path}#{line}",
    sitename="GeneTools.jl",
    format=Documenter.HTML(;
        prettyurls=get(ENV, "CI", "false") == "true",
        canonical="https://bfaber.gitlab.io/GeneTools.jl",
        assets=String[],
    ),
    pages=[
        "Home" => "index.md",
    ],
)
