module GeneTools
using Requires
using Measures
using FortranFiles
using LinearAlgebra
using StaticArrays
using StatsBase
using StructArrays
using ComponentArrays
using DataStructures
using Polyester
using FastGaussQuadrature
using FFTW
using CoordinateTransformations
using Printf
using PlasmaEquilibriumToolkit
using Interpolations
using HDF5

include("base/GeneDict.jl")
include("base/diagnostic_definitions.jl")
include("base/types.jl")

# Set up the diagnostic
# diagState is a MUTABLE struct global to the module, recording the state of the GENE diagnostic
#diag = Diag()

# Include all the base code
include("base/timeslice_data.jl")
include("base/fileOps.jl")
include("base/fileIO.jl")
include("base/diag.jl")
include("base/grid.jl")
include("base/geometry.jl")


# Include all the diagnostic code
include("include/flux_spectra.jl")
include("include/contours.jl")
include("include/field.jl")
include("include/nrg.jl")
include("include/cross_phases.jl")
include("include/eigenvalues.jl")
include("include/ballooning.jl")
include("include/z_profile.jl")
include("include/visualization.jl")

#=
include("include/triplet.jl")
include("include/nrg.jl")
include("include/omega.jl")
include("include/QLHF.jl")
include("include/mom.jl")
# Set up the diagnostic
# diag is a MUTABLE struct global to the module, recording the state of the GENE diagnostic
diag = startGeneDiag()
diagList = startDiagList()
=#

function __init__()
    @require VMEC = "2b46c670-0004-47b5-bf0a-1741584931e9" include("base/vmec_interface.jl")
    @require NE3DLE = "6356e481-0c20-445f-8811-d253bdede59b" include("base/ne3dle_interface.jl")
end

export AbstractGeneDiagnostic, get_fields_and_moments, add_plot_data
export NRG, FluxSpectra

export DiagControl, GeneMomentID, GeneTrapPassMomentID, GeneFieldID, GeneIDVector
export GeneData, OmegaData, EigenvalueData, GeneFileInfo, GeneGrid, GeneGeom 

export GeneTimesliceData, get_gene_data, get_gene_data!, get_file_info, read_parameters_file, get_isteps
export start_gene_diag, reset_diag!, read_diag_file, get_data_times, get_sim_times, run_diags!, run_scan_diags!

export set_grid_coords!, make_real_space_flux_tube_grid
export set_geometry!, read_gist_file!, write_gene_geometry

end
