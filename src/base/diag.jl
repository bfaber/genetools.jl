#using ArgParse

"""
  startGeneDiag()

Initializes the diag struct that is then used to hold all the information
related to GeneDiag
"""
function start_gene_diag(directory::String = pwd();
                         FloatType = Float64,
                        )
  diag = DiagControl{FloatType}()
  diag.mod_dir = directory
  return diag
end

function reset_diag!(diag::DiagControl{T}) where {T}
  diag = DiagControl{T}()
  return diag
end
function reset_diag!(diag::DiagControl{T},FloatType) where {T}
    diag = DiagControl{FloatType}()
    return diag
  end

#=
function parseCommandLine()
  args = ArgParseSettings
  @add_arg_table! args begin
    "file"
      help = "Diagnostic input file"
      required = true
  end
  parsed_args = parse_args(args)
end
=#
function check_precision(diag_file::String,
                         FloatType::Type = Float64,)
    diag = DiagControl{FloatType}()
    include(diag_file)
    f_list = [:run_IDs,:data_path]
    for f in f_list
        @debug f, isdefined(@__MODULE__, f)
        if isdefined(@__MODULE__, f)
            temp_vals = if f === :run_IDs
                expand_file_series(string(eval(f)))
            else
                eval(f)
            end
            setfield!(diag, f, temp_vals)
        end 
    end 
    run = diag.run_IDs[1]
    par_file = joinpath(diag.data_path, "parameters_"*run)
    parameters, _ = isfile(par_file) ? read_parameters_file(par_file) : error("Parameters file $(par_file) does not exist!")
    diag.parameters[run] = parameters
    if parameters[:info][:PRECISION] != FloatType
        FloatType = parameters[:info][:PRECISION]
    end  
    return reset_diag!(diag,FloatType),FloatType
end

function read_diag_file(diag_file::String,
                        FloatType::Type = Float64,
                       )
    diag,FloatType = check_precision(diag_file,FloatType)
    include(diag_file)
    for f in fieldnames(DiagControl)
        @debug f, isdefined(@__MODULE__, f)
        if isdefined(@__MODULE__, f)
            temp_vals = if f === :run_IDs
                expand_file_series(string(eval(f)))
            else
                if typeof(eval(f)) <: AbstractFloat && typeof(eval(f)) != FloatType
                    convert(FloatType,eval(f))
                else
                    eval(f)
                end
            end
            @debug string(eval(f))
            if f === :diagnostics
                eltype(temp_vals) isa eltype(fieldtype(DiagControl, f))  || error("Diagnsotic input \"$(f) = $(temp_vals)\" not a valid option")
                for i in temp_vals
                    push!(getfield(diag, f), i)
                end
            else
                typeof(temp_vals) <: fieldtype(DiagControl, f) || error("Diagnsotic input \"$(f) = $(temp_vals)\" not a valid option")
                setfield!(diag, f, temp_vals)
            end
            
            setfield!(diag,:scan_diagnostics,Dict())
            for run in diag.run_IDs
                diag.scan_diagnostics[run] = copy(diag.diagnostics)
            end
        end
    end


    # Check that diag.dataPath is a legitmate directory
    if ~isdir(diag.data_path)
        throw(ArgumentError("Directory $(diag.data_path) does not exist"))
    end

    # Get a list of all the files in the diag.dataPath directory
    run_files = readdir(diag.data_path)
    filter!(i -> !occursin(r"\.", i), run_files)
    #@debug(run_files)
    
    par_scan_file = joinpath(diag.data_path, "parameters")
    if isfile(par_scan_file)
        diag.scan_parameters = get_scan_parameters(par_scan_file,length(diag.run_IDs))
    end

    # Loop over the runIDs and populate the appropriate parameter dictionaries
    for r in eachindex(diag.run_IDs)
        run = diag.run_IDs[r]
        par_file = joinpath(diag.data_path, "parameters_"*run)
        parameters, species_list = isfile(par_file) ? read_parameters_file(par_file) : error("Parameters file $(par_file) does not exist!")
        diag.parameters[run] = parameters
        diag.species_list = isempty(diag.species_list) ? species_list : sort(species_list) == sort(diag.species_list) ? diag.species_list : throw(ArgumentError("Continued runs must have the same species!"))
        # Find all of the files in diag.dataPath that are associated with the runID
        #n_files = mapreduce(x->occursin("_"*run, x), +, run_files, dims = 1)[1]-1
        #file_check = map(x->occursin("_"*run, x), run_files)
        #sim_files = Array{String}(undef, n_files)
        diag.run_files[run] = filter(i -> occursin(run, i), run_files)
        #@debug diag.run_files[run]
        #n_files = length(run_files)
        #j = 1
        for run_file in diag.run_files[run]
            #@debug run_file
            diag.file_info[run_file] = get_file_info(parameters, joinpath(diag.data_path, run_file))
        end

        for scan_param in keys(diag.scan_parameters)
            for name in keys(diag.parameters[run])
                if haskey(diag.parameters[run][name],scan_param)
                    diag.scan_parameters[scan_param][r] = diag.parameters[run][name][scan_param]
                end
            end
        end        
    end

    # Set the simulation grid and geometry
    diag = set_grid_coords!(diag)
    diag = set_geometry!(diag)

    diag.sim_times, diag.sim_steps, diag.file_steps = get_sim_times(diag)
    # Compute the starting simulation step for analysis
    
    start_step = findfirst(x -> x >= diag.start_time, diag.sim_times)
    end_step = findlast(x -> x <= diag.end_time, diag.sim_times)
    if end_step == length(diag.sim_times)+1
        end_step -= 1
    end
    diag.start_step = diag.sim_steps[start_step]
    diag.end_step = diag.sim_steps[end_step]

    diag.start_time = diag.sim_times[start_step]
    diag.end_time = diag.sim_times[end_step]

    diag.nrg_data = NRG(diag)
    run_diag!(diag, diag.nrg_data)

    nkx = diag.parameters[first(diag.run_IDs)][:box][:nx0]
    nky = diag.parameters[first(diag.run_IDs)][:box][:nky0]
    nz = diag.parameters[first(diag.run_IDs)][:box][:nz0]
    nky2 = 2 * nky

    is_nonlinear = diag.parameters[first(diag.run_IDs)][:general][:nonlinear]

    diag.irfft_plan_xyz = (nkx * nky2) * plan_irfft(Array{Complex{FloatType}}(undef, nkx, nky + 1, nz), nky2, [2, 1])
    diag.irfft_plan_xy = (nkx * nky2) * plan_irfft(Array{Complex{FloatType}}(undef, nkx, nky + 1), nky2, [2, 1])
    diag.irfft_plan_yz = (nky2) * plan_irfft(Array{Complex{FloatType}}(undef, nkx, nky + 1, nz), nky2, [2])
    diag.irfft_plan_y = (nky2) * plan_irfft(Array{Complex{FloatType}}(undef, nkx, nky + 1, nz), nky2, [2])
    diag.ifft_plan_xz = (nkx) * plan_ifft(Array{Complex{FloatType}}(undef, nkx, nky + 1*(is_nonlinear), nz), [1])
    diag.ifft_plan_x = (nkx) * plan_ifft(Array{Complex{FloatType}}(undef, nkx, nky + 1*(is_nonlinear)), [1])
    #diag.plotType=="NERSC" ? include("src/base/plot.jl") : include("src/base/pyplot.jl")

    diag_exprs = diag.diagnostics
    for i in eachindex(diag_exprs)
        ex = diag_exprs[i]
        ex_args = filter(e -> !Meta.isexpr(e, :kw), ex.args[2:end])
        ex_kw_args = filter(e -> Meta.isexpr(e, :kw), ex.args[2:end])
        @debug ex_args
        @debug ex_kw_args
        #insert!(ex.args, 2, diag)
        #diag.diagnostics[i] = eval(ex)
        new_diag = eval(Expr(:call, ex.args[1], diag, ex.args[2:end]...))
        @debug typeof(new_diag)
        diag.diagnostics[i] = eval(Expr(:call, ex.args[1], diag, ex.args[2:end]...))
    end
    for run in diag.run_IDs
        diag_exprs = diag.scan_diagnostics[run]
        for i in eachindex(diag_exprs)
            ex = diag_exprs[i]
            ex_args = filter(e -> !Meta.isexpr(e, :kw), ex.args[2:end])
            ex_kw_args = filter(e -> Meta.isexpr(e, :kw), ex.args[2:end])
            @debug ex_args
            @debug ex_kw_args
            #insert!(ex.args, 2, diag)
            #diag.diagnostics[i] = eval(ex)
            new_diag = eval(Expr(:call, ex.args[1], diag, ex.args[2:end]...))
            @debug typeof(new_diag)
            diag.scan_diagnostics[run][i] = eval(Expr(:call, ex.args[1], diag, ex.args[2:end]...))
        end
    end
    
    get_data_times!(diag)

    return diag
end


"""
  read_diag_file!(diag::DiagControl, diag_par_file::String)

Reads a diagnostic input file, which should set in the same directory as the
top level GeneDiag.jl file.  The entries of the diagnostic file should be labeled
by the fields of the Diag struct.  Only a subset of the Diag fields needs to be
populated in the diagnostic input file, if a field is not listed in the diagnostic
input file, then that field value won't be changed in the Diag object.
"""
function read_diag_file!(diag::DiagControl{T},
                         diag_control_file::String,
                        ) where {T}
    reset_diag!(diag)
    # Open the diag input file for reading
    file = open(diag_control_file,"r")

    # Create an empty dict with the diag file values
    diag_pars = Dict()
    # Read each line, splitting on the = char, add the key and value to the diagPars dict
    for line in eachline(file)
        line_split = split(line,"=")
        if length(line_split) == 2
            diag_key = strip(line_split[1])
            diag_val = strip(line_split[2])
            diag_pars[diag_key] = diag_val
        elseif length(line_split) > 2
            diag_key = strip(line_split[1])
            diag_val = strip(join(line_split[2:end], "="))
            diag_pars[diag_key] = diag_val
        end
    end
    close(file)

    #=
    This is the meat of the function.
    Each key in the diag input file should map to a field in the diag struct
    =#
    diag_field_names = [string(i) for i in fieldnames(DiagControl)]
    for (key, val) in diag_pars
        # Check if a the key exists in diagPars
        if key in diag_field_names
            # Convert the key::String to a Symbol
            field_symbol = Symbol(key)
            # Get the type of the field, and if an array, the type of the array element
            field_type = fieldtype(DiagControl, field_symbol) <: AbstractArray ? eltype(fieldtype(DiagControl, field_symbol)) : fieldtype(DiagControl, field_symbol)
            
            # Use convertArrayFromString1D to parse the diag input and return the appropriate value
            temp_vals = Array{field_type}(undef, 0)
            temp_vals = if field_symbol === :run_IDs
                expand_file_series(val)
            elseif field_symbol === :diags
                expr = Meta.parse(val)
                all(i -> eval(i.args[1]) <: AbstractGeneDiagnostic, expr.args) || error("Incorrect diagnostic specification")
                diag_list = Vector{UnionAll}()
                for (i, diag_expr) in enumerate(expr.args)
                    @debug diag_expr.args[2:end]
                    diag_args = diag_expr.args[2:end]
                    diag_head = diag_expr.args[1]
                    local_expr = Expr(:call, diag_head, diag, diag_args...)
                    @debug typeof(local_expr)
                    eval(diag_args...)
                    #local_diag = eval(local_expr)
                end
                diag_list
            else
                eval(Meta.parse(val))
            end
            @debug(field_symbol, val, temp_vals, eltype(temp_vals))

            setfield!(diag, field_symbol, temp_vals)
        else
            throw(ArgumentError("Field $key is not a valid diagnostic parameter"))
        end
    end

    # Check that diag.dataPath is a legitmate directory
    if ~isdir(diag.data_path)
        throw(ArgumentError("Directory $(diag.data_path) does not exist"))
    end

    # Get a list of all the files in the diag.dataPath directory
    run_files = readdir(diag.data_path)
    filter!(i -> !occursin(r"\.", i), run_files)
    @debug(run_files)
    # Loop over the runIDs and populate the appropriate parameter dictionaries
    for run in diag.run_IDs
        par_file = joinpath(diag.data_path, "parameters_"*run)
        parameters, species_list = isfile(par_file) ? read_parameters_file(par_file) : error("Parameters file $(par_file) does not exist!")
        diag.parameters[run] = parameters
        diag.species_list = isempty(diag.species_list) ? species_list : sort(species_list) == sort(diag.species_list) ? diag.species_list : throw(ArgumentError("Continued runs must have the same species!"))

        diag.run_files[run] = filter(i -> occursin(run, i), run_files)
        @debug diag.run_files[run]

        for run_file in diag.run_files[run]
            diag.file_info[run_file] = get_file_info(parameters, joinpath(diag.data_path, run_file))
        end
    end

    # Set the simulation grid and geometry
    diag = set_grid_coords!(diag)
    diag = setGeometry!(diag)

    diag.sim_times, diag.sim_steps, diag.file_steps = get_sim_times(diag)

    # Compute the starting simulation step for analysis
    start_step = findfirst(x -> x >= diag.start_time, diag.sim_times)
    end_step = findlast(x -> x <= diag.end_time, diag.sim_times)
    diag.start_step = diag.sim_steps[start_step]
    diag.end_step = diag.sim_steps[end_step]

    diag.start_time = diag.sim_times[start_step]
    diag.end_time = diag.sim_times[end_step]

    #diag.plotType=="NERSC" ? include("src/base/plot.jl") : include("src/base/pyplot.jl")

    return diag
end


"""
    get_data_times!(diag::DiagControl)

Determines the simulation timesteps to be read when reading from the data files.
The timesteps will be separated by the least common multiple
of the `isteps` for each of the relevant data files for a run 
multiplied by the `sparse_step`. If a file does not have any
timesteps between `diag.start_time` and `diag.end_time` then
then the corresponding entry in `diag.record_times` will be empty.
"""
function get_data_times!(diag::DiagControl)
# Scan through the diagnostics and find the which files need to be read
# Use the lcm(x,y) function to compute the sparse step based on the input sparseStep
    diag.record_times = empty(diag.record_times)
    sparse_step = diag.sparse_step
    data_time_steps = Vector{Int}()

    file_list = Vector{Symbol}()
    for diagnostic in diag.diagnostics
        push!(file_list, fields(eval(diagnostic))...)
    end
    unique!(file_list)
    
    
    sparse_step_offset = sparse_step
    offset_step = 0
    for run in diag.run_IDs
        total_steps = diag.parameters[run][:info][:number_of_computed_time_steps]
        if diag.start_step <= offset_step + total_steps
            starting_step = diag.start_step - offset_step
            ending_step = diag.end_step - offset_step
            step_size = lcm(get_isteps(diag, run, file_list))
            record_times = collect(range(start = 1, step = step_size, stop = total_steps + 1))
            if mod(total_steps, step_size) == 0 && run != last(diag.run_IDs)
                deleteat!(record_times, length(record_times))
            end
            
            if diag.last_time_step
                start_step = length(record_times)
                stop_step = length(record_times)
            else
                start_step = findfirst(r -> r >= starting_step, record_times)
                stop_step = findlast(r -> r <= ending_step, record_times)
            end

            n_steps = div(stop_step - start_step + 1, sparse_step)
            
            @debug start_step, start_step + sparse_step - sparse_step_offset
            sparse_record_times = collect(range(start = start_step + sparse_step - sparse_step_offset, 
                                                step = sparse_step, length = n_steps))
            diag.record_times[run] = record_times[sparse_record_times]
            sparse_step_offset = sparse_step > 1 ? mod(length(record_times) - start_step, sparse_step) : 1
            @debug sparse_step_offset
        else
            diag.record_times[run] = Vector{Int}()
        end
        offset_step += total_steps
    end
end

"""
    function get_sim_times(diag::Diag)

Reads the times from a (series) of nrg files for the `"run_IDs"`
specified in the `DiagControl` struct, producing a vector of times
and corresponding simulation timesteps.
"""
function get_sim_times(diag::DiagControl{T}) where {T}
    # Initialize empty data structure
    times = Vector{T}()
    timesteps = Vector{Int}()
    file_steps = Dict{String, StepRange{Int, Int}}()
    offset_step = 0
    for run in diag.run_IDs
        # Check whether every run exists
        #nrg_file = isfile(joinpath(data_path,"nrg_"*run)) ? joinpath(data_path,"nrg_"*run) : throw(ArgumentError("nrg file nrg_$(run) does not exist!"))
        #nrg_par_dict = diag.parameters[run]
        nrg_info = diag.file_info["nrg_"*run]
        nrg_step_size = diag.parameters[run][:in_out][:istep_nrg]
        total_steps = if diag.parameters[run][:info][:number_of_computed_time_steps] > 0
            diag.parameters[run][:info][:number_of_computed_time_steps]
        else
            nrg_lines = countlines(nrg_info.filename)
	    # Subtract one for t = 0
            nrg_steps = div(nrg_lines, length(diag.species_list)+1)-1
	    diag.parameters[run][:info][:number_of_computed_time_steps] = nrg_steps * nrg_step_size
            nrg_steps * nrg_step_size
        end
        n_steps = div(total_steps, nrg_step_size) + 1
        # Read all the entries in the nrg file
        record_locs = collect(range(start = 1, stop = nrg_info.records))
        #field_locs = collect(range(start = 1, stop = nrg_info.data_dims[end]))
        field_locs = [1]
        #times, data = readGeneTextFile(Array{Int}(range(1,stop=nrgInfo.timesteps)),nrgInfo)
        data_array = Array{nrg_info.DataType}(undef, Tuple(nrg_info.block_dims))
	nrg_times = read_gene_nrg_file!(data_array, nrg_info, record_locs,
                                        field_locs; find_times = true)
        
        #deal with duplicate times from continued runs
        nrg_step_range = range(start = offset_step + 1, step = nrg_step_size, length = n_steps)
        nrg_timesteps = collect(nrg_step_range)
        
        if (mod(total_steps, nrg_step_size) == 0) && (run != last(diag.run_IDs))
            deleteat!(nrg_timesteps, length(nrg_timesteps))
            deleteat!(nrg_times, length(nrg_times))
            nrg_step_range = range(start = offset_step + 1, step = nrg_step_size, length = n_steps - 1)
        end
        file_steps[run] = nrg_step_range
        times = vcat(times, nrg_times)
        timesteps = vcat(timesteps, nrg_timesteps)
        
        offset_step += total_steps
    end
    return times, timesteps, file_steps
end


function run_diags!(diag::DiagControl)
    diag.record_times = Dict{String, Vector{Int}}()
    get_data_times!(diag)
    
    which_species = Symbol[]
    which_fields = Symbol[]
    which_vars = Symbol[]

    for d in diag.diagnostics
        if species(d) != (:none,)
            push!(which_species, species(d)...)
            push!(which_fields, fields(d)...)
            push!(which_vars, get_fields_and_moments(d)...)
        end
    end
    which_species = tuple(unique(which_species)...)
    which_fields = tuple(unique(which_fields)...)
    which_vars = unique(which_vars)

    which_species, which_fields, which_vars

    for run in diag.run_IDs
        if !isempty(diag.record_times[run])
            t_data = GeneTimesliceData(diag, run, which_species, which_fields, which_vars)
            for t in diag.record_times[run]
                t_data.timestep[1] = t
                println("Evaluating time $(t_data.timestep[])")
                GeneTools.get_gene_data!(t_data)
                for d in eachindex(diag.diagnostics)
                    if !diag.time_average
                        reset_plot_data!(diag.diagnostics[d])
                    end
                    if species(diag.diagnostics[d]) == (:none,)
                        run_diag!(diag.diagnostics[d], diag, run)
                    else
                        run_diag!(diag.diagnostics[d], diag, t_data)
                    end
                end
            end
        end
    end
    if diag.time_average
        n_times = n_timesteps(diag)
        for d in diag.diagnostics
            time_average!(d, n_times)
        end
    end
    #=
    for diagnostic in diag.diags
        diag_fields = field_strings(diagnostic)
        data_fields, data_moments = get_fields_and_moments(diagnostic)
        for run in diag.run_IDs
            if !isempty(diag.record_times[run])
                for field in diag_fields
                    #file_string = field*_*run
                    diag.data_files[file_string] = get_gene_data(diag.file_info[file_string], 
                                                                 diag.record_times[run],
                                                                 data_fields)
                end
            end
        end
    end
    =#
end

function run_scan_diags!(diag::DiagControl)
    diag.record_times = Dict{String, Vector{Int}}()
    get_data_times!(diag)
     
    for run in diag.run_IDs
        which_species = Symbol[]
        which_fields = Symbol[]
        which_vars = Symbol[]

        for d in diag.scan_diagnostics[run]
            if species(d) != (:none,)
                push!(which_species, species(d)...)
                push!(which_fields, fields(d)...)
                push!(which_vars, get_fields_and_moments(d)...)
            end
        end

        which_species = tuple(unique(which_species)...)
        which_fields = tuple(unique(which_fields)...)
        which_vars = unique(which_vars)
        if !isempty(which_species)
            if !isempty(diag.record_times[run])
                t_data = GeneTimesliceData(diag, run, which_species, which_fields, which_vars)
                for t in diag.record_times[run]
                    t_data.timestep[1] = t
                    println("Evaluating time $(t_data.timestep[])")
                    GeneTools.get_gene_data!(t_data)
                    for d in eachindex(diag.diagnostics)
                        if !diag.time_average
                            reset_plot_data!(diag.scan_diagnostics[run][d])
                        end
                        #Ham-fisted way of flipping to eigenvalue diagnostic
                        if species(diag.scan_diagnostics[run][d]) == (:none,)
                            run_diag!(diag.scan_diagnostics[run][d], diag, run)
                        else
                            run_diag!(diag.scan_diagnostics[run][d], diag, t_data)
                        end
                    end
                end
            end
        else
            for d in eachindex(diag.diagnostics)
                if !diag.time_average
                    reset_plot_data!(diag.scan_diagnostics[run][d])
                end       
                run_diag!(diag.scan_diagnostics[run][d], diag, run)  
            end
        end
    end
    if diag.time_average
        for run in diag.run_IDs
            n_times = n_timesteps(diag,run)
            for d in diag.scan_diagnostics[run]
                time_average!(d, n_times)
            end
        end
    end
    #=
    for diagnostic in diag.diags
        diag_fields = field_strings(diagnostic)
        data_fields, data_moments = get_fields_and_moments(diagnostic)
        for run in diag.run_IDs
            if !isempty(diag.record_times[run])
                for field in diag_fields
                    #file_string = field*_*run
                    diag.data_files[file_string] = get_gene_data(diag.file_info[file_string], 
                                                                 diag.record_times[run],
                                                                 data_fields)
                end
            end
        end
    end
    =#
end


function get_sim_data(diag::DiagControl)
  readRecs=map(i->i,range(1,stop=gFile.records))
  readFields=map(i->i,range(1,stop=gFile.dataDims[length(gFile.dataDims)]))
end

function run_diag()
  parsedArgs = parseCommandLine()
  file = parsedArgs["file"]
  runDir = getDiagModDir()
  inputDir = isempty(dirname(file)) ? runDir : dirname(file)
  diagParFile = isfile(inputDir*basename(file)) ? inputDir*basename(file) : throw(ArgumentError("Diagnostic input file does not exist"))

  readDiagFile(diag,diagParFile)
  return nothing
end

# Ordered dictionary of multiple runs to make nice comparison plots
function startDiagList()
  diagList = DiagList()
  return diagList
end

function addToDiagList!(diag)
  diagList.diagDict[diag.dataPath] = diag
  diagList.num_diags += 1
end

function n_timesteps(diag::DiagControl)
    return sum(length.(values(diag.record_times)))
end

function n_timesteps(diag::DiagControl,run_ID::String)
    return sum(length.(values(diag.record_times[run_ID])))
end

function save_plot_data(diag::DiagControl)
    for d in diag.diagnostics
        for (j, s) in enumerate(species(d))
            name = get_diag_name(d)
            fid_name = s == :none ? diag.out_path * name * "_scan_" * String(s) * ".h5" : diag.out_path * name * "_" * String(s) * "_" * diag.run_IDs[1] * ".h5"
            if isfile(fid_name)
                rm(fid_name)
            end
	    println(fid_name)
            fid = h5open(fid_name, "cw") 
            save_plot_data(d,j,fid)
            close(fid)
        end
    end
end

function save_scan_plot_data(diag::DiagControl)
    for d in eachindex(diag.diagnostics)
        for (j, s) in enumerate(species(diag.diagnostics[d]))
            name = get_diag_name(diag.diagnostics[d])
            fid_name = s == :none ? diag.out_path * name * "_scan.h5" : diag.out_path * name * "_scan_" * String(s) * ".h5"
            if isfile(fid_name)
                rm(fid_name)
            end
            fid = h5open(fid_name, "cw")
            for scan_par in keys(diag.scan_parameters)
                string_par = String(scan_par)
                fid[string_par] = diag.scan_parameters[scan_par]
            end
            for r in eachindex(diag.run_IDs)
                run = diag.run_IDs[r]
                save_plot_data(diag.scan_diagnostics[run][d],j,fid,run)
            end
            close(fid)
        end
    end
end
