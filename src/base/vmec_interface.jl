using .VMEC
export gene_geometry_coefficients

# Functions for write_gene_geometry abstraction
function Ba(eq::VmecSurface)
    return abs(eq.phi[end]/(π*eq.Aminor_p^2))
end

function s0(eq::VmecSurface, coords)
    return getfield(coords[1], 1)*2π/eq.phi[end]*eq.signgs
end

function iota(eq::VmecSurface)
    return eq.iota[1]
end   

function shat(eq::VmecSurface ,coords)
    q0 = 1/eq.iota[1]
    return -2*s0(eq, coords)/q0*eq.iota[2]*q0^2
end

function R_major(eq::VmecSurface)
    return eq.Rmajor_p
end

function A_minor(eq::VmecSurface)
    return eq.Aminor_p
end

function dpdx(eq::VmecSurface, coords)
    return -4.0*sqrt(s0(eq,coords))*eq.pres[2]/(Ba(eq)^2)*4π*1e-7
end

"""
    (::GeneFromPest)(p::PestCoordinates, vmec_surface::VmecSurface)

Transform coordinates to the GENE coordinate system
# x = ``sqrt(ψ*2π/ψ_lcfs)``
# ∇x = ∇ψ dx/dψ = ∇ψ 1/(2 x) 2π / ψ_lcfs
# y = x * αₚ
# ∇y = ∇(αₚ/ι)|ι₀|x₀
#    = x(∇αₚ + αₚ * 2π/ψ_lcfs * ι'/ι * ∇ψ)
# z = αₚ - ι * ζ
# ∇z = ∇αₚ - ∇ι * ζ - ι * ∇ζ
#    = ∇αₚ - 2π/ψ_lcfs * ι' * ∇ψ * ζ - ι * ∇ζ
"""
function (::GeneFromPest)(p::PestCoordinates,
                          vmecsurf::VmecSurface;
                         )
  Φ = vmecsurf.phi[end]*vmecsurf.signgs
  x = sqrt(p.ψ*2π/Φ)
  y = x*p.α
  z = p.α - vmecsurf.iota[1] * p.ζ
  return GeneCoordinates(x, y, z)
end




function PlasmaEquilibriumToolkit.transform_basis(::GeneFromPest,
                                                  p::PestCoordinates,
                                                  e::BasisVectors,
                                                  vmecsurf::VmecSurface;
                                                 )
  Aminor = vmecsurf.Aminor_p
  Φ = vmecsurf.phi[end]*vmecsurf.signgs
  x = sqrt(p.ψ*2π/Φ)
  ∇x = Aminor*π/(Φ*x)*e[:,1]
  # Multiple by sign(Φ) to ensure coordinate system is left handed
  ∇y = sign(Φ) * Aminor * x * (e[:,2] + p.α/vmecsurf.iota[1] * vmecsurf.iota[2] * 2π/Φ * e[:, 1])
  ∇z = Aminor*(e[:,2] - p.ζ*vmecsurf.iota[2]*2π/Φ*e[:,1] - vmecsurf.iota[1]*e[:,3])
  return hcat(∇x, ∇y, ∇z)
end

# ϕ: toroidal flux as a function of poloidal pest ψ
# ϕ(ψ*2π/ψ_lcfs)
# x = sqrt(ϕ(ψ*2π/ψ_lcfs)/ϕ_lcfs)
# ∇x = ∇ψ dx/dψ = ∇ψ π ϕ' / (ψ_lcfs * ϕ_lcfs * x)
# y = x * αₚ
# ∇y = ∇(αₚ/ι)|ι₀|x₀
#    = x(∇αₚ + αₚ * 2π/ψ_lcfs * ι'/ι * ∇ψ)
# z = αₚ - ι * ζ
# ∇z = ∇αₚ - ∇ι * ζ - ι * ∇ζ
#    = ∇αₚ - 2π/ψ_lcfs * ι' * ∇ψ * ζ - ι * ∇ζ
#=
function PlasmaEquilibriumToolkit.transform_basis(::GeneFromPesp,
        p::PespCoordinates,
        e::BasisVectors,
        vmecsurf::VmecSurface;
        )
Aminor = vmecsurf.Aminor_p
    χ = vmecsurf.chi[end]*vmecsurf.signgs
x = sqrt(p.Φ * vmecsurf.chi[1]/vmecsurf.chi[end])
    ∇x = Aminor*π*vmecsurf.phi[2] / (χ * vmecsurf.phi[end] * x)*e[:,1]
# Multiple by sign(ξ) to ensure coordinate system is left handed
    ∇y = sign(χ) * Aminor * x * (e[:,2] + p.α/vmecsurf.iota[1] * vmecsurf.iota[2] * 2π/χ * e[:, 1])
    ∇z = Aminor*(e[:,2] - p.ζ*vmecsurf.iota[2]*2π/χ*e[:,1] - vmecsurf.iota[1]*e[:,3])
return hcat(∇x, ∇y, ∇z)
    end
=#


"""
    gene_geometry_coefficients(::GeneFromPest, p::PestCoordinates, vmec_surf::VmecSurface)
    gene_geometry_coefficients(::GeneFromPest, p::StructArray{PestCoordinates}, vmec_surf::VmecSurface)

Compute the normalization geometric quantities for a GENE simulation
from a point or set of points specfied in `PestCoordinates` on a `VMEC`surface.
"""
function gene_geometry_coefficients(::GeneFromPest,
                                    p::PestCoordinates,
                                    vmecsurf::VmecSurface;
                                   )
  Aminor = vmecsurf.Aminor_p
  Ba = abs(vmecsurf.phi[end])/(π*Aminor^2)
  v = VmecFromPest()(p,vmecsurf)
  vmecContraBasis = basis_vectors(Contravariant(), CartesianFromVmec(), v, vmecsurf)
  pestContraBasis = transform_basis(PestFromVmec(), v, vmecContraBasis,vmecsurf)
  geneContraBasis = transform_basis(GeneFromPest(), p, pestContraBasis,vmecsurf)
  geneCoBasis = transform_basis(CovariantFromContravariant(), geneContraBasis)
  gB = grad_B(v, vmecContraBasis, vmecsurf)
  geneMetric = metric(geneContraBasis)
  BnormMag = norm(cross(geneContraBasis[:,1], geneContraBasis[:,2]))
  # The Jacobian should always be negative, but as only the integral measure
  # is required, that must be positive negative
  jac = abs(1.0/dot(geneContraBasis[:,1], cross(geneContraBasis[:,2], geneContraBasis[:,3])))
  K1, K2 = Aminor/Ba.*grad_B_projection(geneContraBasis,gB)
  dBdZ = Aminor/Ba*dot(gB, geneCoBasis[:,3])
  # The GENE K1 component is the negative of what is computed, see the GIST documentation note
  return geneMetric, BnormMag, abs(jac), -K1, K2, dBdZ
end

#=
function gene_geometry_coefficients(::GeneFromPesp,
                                  p::PespCoordinates,
                                  vmecsurf::VmecSurface;
                                 )
  Aminor = vmecsurf.Aminor_p
  Ba = abs(vmecsurf.phi[end])/(π*Aminor^2)
  v = VmecFromPesp()(p,vmecsurf)
  vmecContraBasis = basis_vectors(Contravariant(), CartesianFromVmec(), v, vmecsurf)
  pespContraBasis = transform_basis(PespFromVmec(), v, vmecContraBasis,vmecsurf)
  geneContraBasis = transform_basis(GeneFromPesp(), p, pespContraBasis,vmecsurf)
  geneCoBasis = transform_basis(CovariantFromContravariant(), geneContraBasis)
  gB = grad_B(v, vmecContraBasis, vmecsurf)
  geneMetric = metric(geneContraBasis)
  BnormMag = norm(cross(geneContraBasis[:,1], geneContraBasis[:,2]))
  # The Jacobian should always be negative, but as only the integral measure
  # is required, that must be positive negative
  jac = abs(1.0/dot(geneContraBasis[:,1], cross(geneContraBasis[:,2], geneContraBasis[:,3])))
  K1, K2 = Aminor/Ba.*grad_B_projection(geneContraBasis,gB)
  dBdZ = Aminor/Ba*dot(gB, geneCoBasis[:,3])
  # The GENE K1 component is the negative of what is computed, see the GIST documentation note
  return geneMetric, BnormMag, abs(jac), -K1, K2, dBdZ
end
=#

function gene_geometry_coefficients(::GeneFromPest,
                                    p::StructArray{PestCoordinates{T,T}},
                                    vmecsurf::VmecSurface
                                   ) where {T}
  gene_metric = Array{SVector{6,Float64}}(undef, size(p))
  K1 = Array{Float64}(undef, size(p))
  K2 = similar(K1)
  dBdZ = similar(K1)
  B_norm_mag = similar(K1)
  jac = similar(K1)
  for i in eachindex(p)
    gene_metric[i], B_norm_mag[i], jac[i], K1[i], K2[i], dBdZ[i] = gene_geometry_coefficients(GeneFromPest(),p[i],vmecsurf)
  end
  return gene_metric, B_norm_mag, jac, K1, K2, dBdZ
end
#=
function gene_geometry_coefficients(::GeneFromPesp,
                                  p::StructArray{PespCoordinates{T,T}},
                                  vmecsurf::VmecSurface
                                 ) where {T}
  geneMetric = Array{SVector{6,Float64}}(undef, size(p))
  K1 = Array{Float64}(undef, size(p))
  K2 = similar(K1)
  dBdZ = similar(K1)
  B_norm_mag = similar(K1)
  jac = similar(K1)
  @batch for i in eachindex(p)
    geneMetric[i], B_norm_mag[i], jac[i], K1[i], K2[i], dBdZ[i] = gene_geometry_coefficients(GeneFromPesp(),p[i],vmecsurf)
  end
  return geneMetric, B_norm_mag, jac, K1, K2, dBdZ
end

function gene_geometry_coefficientsPol(::GeneFromPest,
                                     p::StructArray{PestCoordinates{T,T}},
                                     vmecsurf::VmecSurface,
                                     vmec::Vmec
                                    ) where {T}
  gene_metric = Array{SVector{6,Float64}}(undef, size(p))
  K1 = Array{Float64}(undef, size(p))
  K2 = similar(K1)
  dBdZ = similar(K1)
  B_norm_mag = similar(K1)
  jac = similar(K1)
  @batch for i in eachindex(p)
    gene_metric[i], B_norm_mag[i], jac[i], K1[i], K2[i], dBdZ[i] = gene_geometry_coefficientsPol(GeneFromPest(),p[i],vmecsurf,vmec)
  end
  return gene_metric, B_norm_mag, jac, K1, K2, dBdZ
end
=#

function write_gene_geometry(woutfile::String,
                             s0::T,
                             alpha0::Union{T,Vector{T}},
                             nz0::Int,
                             pol_turns::Int,
                             filename::String;
                            ) where T
    vmec = readVmecWout(wout)
    write_gene_geometry(vmec, s0, alpha0, nz0, pol_turns, filename)
end

function write_gene_geometry(vmec::Vmec,
                             s0::T,
                             α₀::Union{T,Vector{T}},
                             nz0::Int,
                             pol_turns::Int,
                             filename::String;
                            ) where T
    vmec_surf = VmecSurface(s0,vmec)

    # VMEC ι is left-handed, PEST ι is right handed
    ι = -vmec_surf.iota[1]

    ζ_range = range(α₀-pol_turns*π/ι,step=2*π*pol_turns/nz0/ι,length=nz0)

    points = MagneticCoordinateCurve(PestCoordinates,vmec_surf.phi[end]*s0*vmec_surf.signgs/(2π),-ι*α₀,ζ_range)

    metric, modB, sqrtg, K1, K2, ∂B = gene_geometry_coefficients(GeneFromPest(),points,vmec_surf)
    write_gene_geometry(filename, points, vmec_surf, metric, modB, sqrtg, K1, K2, ∂B)
end
