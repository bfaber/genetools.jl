abstract type AbstractGeneDiagnostic{T, S, F} end;


function Base.eltype(::AbstractGeneDiagnostic{T, S, F}) where {T, S, F}
    return T
end

function Base.size(x::D) where {D <: AbstractGeneDiagnostic}
    return size(x.data)
end

function species(::AbstractGeneDiagnostic{T, S, F}) where {T, S, F}
    return S
end

function fields(::AbstractGeneDiagnostic{T, S, F}) where {T, S, F}
    return Tuple(F)
end

function field_strings(::AbstractGeneDiagnostic{T, S, F}) where {T, S, F}
    return string.(F)
end

function Base.getindex(x::D, args...) where {D <: AbstractGeneDiagnostic}
    return x.data[args...]
end

function get_fields_and_moments(::AbstractGeneDiagnostic{T, S, F}) where {T, S, F}
end

function add_plot_data!(d::AbstractGeneDiagnostic{T, S, F},
                        field::Symbol,
                        inds::Vector,
                        data::AbstractArray;
                       ) where {T, S, F}
    plot_data_view = view(d.plot_data, field)
    if iszero(ndims(plot_data_view))
        plot_data_view[][inds...] .+= data
    else
        plot_data_view[inds...] .+= data
    end
    return nothing
end

function add_plot_data!(d::AbstractGeneDiagnostic{T, S, F},
                        field::Symbol,
                        inds::Vector,
                        data::Float64;
                       ) where {T, S, F}
    plot_data_view = view(d.plot_data, field)

    if iszero(ndims(plot_data_view))
        plot_data_view[][inds...] .+= data
    else
        plot_data_view[inds...] .+= data
    end
    return nothing
end
function add_plot_data!(d::AbstractGeneDiagnostic{T, S, F},
                        field::Symbol,
                        inds::Vector,
                        data::T;
                       ) where {T, S, F}
    plot_data_view = view(d.plot_data, field)

    if iszero(ndims(plot_data_view))
        plot_data_view[][inds...] .+= data
    else
        plot_data_view[inds...] .+= data
    end
    return nothing
end

function reset_plot_data!(d::AbstractGeneDiagnostic{T, S, F}) where {T, S, F}
    for plot_field in keys(d.plot_data)
        fill!(d.plot_data[plot_field], zero(T))
    end
end

function time_average!(d::AbstractGeneDiagnostic, n_times::Integer)
    for plot_field in keys(d.plot_data)
        d.plot_data[plot_field] ./= n_times
    end
end

# Define specific diagnostic structures here
struct NRG{T, S, F} <: AbstractGeneDiagnostic{T, S, F}
    plot_data::ComponentArray
    times::Vector{T}
    nrg_list::Tuple
    which_files::Vector{Bool}
end


struct FluxSpectra{T, S, F} <: AbstractGeneDiagnostic{T, S, F}
    data::ComponentArray{T}
    plot_data::ComponentArray{T}
    flux_list::Tuple
    kx::Tuple
    ky::Tuple
    z::Tuple
    u_ExB_x::Array{Complex{T}}
    B_x::Array{Complex{T}}
    A::Array{Complex{T}}
    B::Array{Complex{T}}
end

struct Contours{T, S, F} <: AbstractGeneDiagnostic{T, S, F}
    data::Array{Complex{T}}
    plot_data::ComponentArray{T}
    var_list::Tuple
    z::Tuple
end

struct CrossPhases{T, S, F} <: AbstractGeneDiagnostic{T, S, F}
    data::Array{Complex{T}}
    plot_data::ComponentArray{T}
    var_list::Vector{Tuple}
    z::Tuple
    ky_vec::Vector{T}
end

struct Eigenvalues{T, S, F} <: AbstractGeneDiagnostic{T, S ,F}
    plot_data::ComponentArray{T}
    var_list::Vector{Symbol}
end

struct Ballooning{T, S, F} <: AbstractGeneDiagnostic{T, S, F}
    data::Array{Complex{T}}
    plot_data::ComponentArray{Complex{T}}
    var_list::Tuple
    norm::Bool
    z_ext::Vector{T}
end

struct ZProfile{T, S, F} <: AbstractGeneDiagnostic{T, S, F}
    data::Array{Complex{T}}
    plot_data::ComponentArray{T}
    var_list::Tuple
    norm::Bool
    non_zonal::Bool
    kx::Vector{Int}
    ky::Vector{Int}
    z::Vector{T}
end
