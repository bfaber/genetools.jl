
struct GeneTimesliceData{T, S, F}
    run_ID::String
    file_info::Dict{String, GeneFileInfo}
    timestep::Vector{Int}
    data_fields::GeneIDVector
    data::Dict{String, ComponentArray}
end


function GeneTimesliceData(diag::DiagControl{T},
                           run_ID::AbstractString,
                           species_list::NTuple{N, Symbol},
                           read_files::Union{Vector{Symbol}, NTuple{S, Symbol}},
                           data_fields::Union{Vector{Symbol}, NTuple{S, Symbol}},
                           timestep = first(diag.record_times[run_ID]),
                          ) where {N, T, S}
    files = Dict{String, GeneFileInfo}()
    n_records = length(diag.record_times[run_ID])
    is_nonlinear = diag.parameters[first(diag.run_IDs)][:general][:nonlinear]
    for file in diag.run_files[run_ID]
        files[file] = diag.file_info[file]
    end
    field_IDs = eval.(data_fields)
    data = Dict{String, ComponentArray}()
    for file in read_files
        if file === :mom
            for s in species_list
                filename = "mom_"*string(s)
                which_fields = findall(f -> (f isa GeneMomentID) || (f isa GeneTrapPassMomentID), field_IDs)
                if !isempty(which_fields)                    
                    data_dims = diag.file_info[filename*"_"*run_ID].data_dims
                    if diag.file_info[filename*"_"*run_ID].with_trapping
                        trp_fields = Vector{Symbol}(undef, 3 * length(which_fields))
                        for i in eachindex(which_fields)
                            trp_fields[i] = Symbol(data_fields[Int(which_fields[i])], :_trap)
                            trp_fields[length(which_fields) + i] = Symbol(data_fields[Int(which_fields[i])], :_pass)
                            trp_fields[2 * length(which_fields) + i] = Symbol(data_fields[Int(which_fields[i])], :_flr)
                        end
                        axis_args = map(i -> Expr(:kw, trp_fields[i], i), eachindex(trp_fields))
                        ax = eval(Expr(:call, :Axis, axis_args...))
                        data[filename] = ComponentArray(map(i -> zeros(Complex{T}, data_dims[1], data_dims[2] + 1*is_nonlinear, data_dims[3]), eachindex(trp_fields)), ax)
                    else
                        axis_args = map(i -> Expr(:kw, data_fields[Int(which_fields[i])], i), eachindex(which_fields))
                        ax = eval(Expr(:call, :Axis, axis_args...))
                        data[filename] = ComponentArray(map(i -> zeros(Complex{T}, data_dims[1], data_dims[2] + 1*is_nonlinear, data_dims[3]), eachindex(which_fields)), ax)
                    end
                end
            end
        elseif file === :field
            filename = "field"
            which_fields = findall(f -> f isa GeneFieldID, field_IDs)
            if !isempty(which_fields)
                axis_args = map(i -> Expr(:kw, data_fields[Int(which_fields[i])], i), eachindex(which_fields))
                ax = eval(Expr(:call, :Axis, axis_args...))
                data_dims = diag.file_info[filename*"_"*run_ID].data_dims
                data[filename] = ComponentArray(map(i -> zeros(Complex{T}, data_dims[1], data_dims[2] + 1*is_nonlinear, data_dims[3]), eachindex(which_fields)), ax)
            end
        end
    end
    #field_IDs = eval.(data_fields)
    #@debug data
    return GeneTimesliceData{T, species_list, read_files}(run_ID, files, [timestep], field_IDs, data)
end


function species(::GeneTimesliceData{T, S, F}) where {T, S, F}
    return S
end

function get_gene_data!(run_data::GeneTimesliceData{T, S, F}) where {T, S, F}
    for s in species(run_data)
        mom_IDs = filter(f -> (f isa GeneMomentID) || (f isa GeneTrapPassMomentID), run_data.data_fields)
        @debug mom_IDs
        if !isempty(mom_IDs)
            file_id = "mom"*"_"*string(s)
            filename = file_id*"_"*run_data.run_ID
            file_stream = if run_data.file_info[filename].access == "sequential"
                FortranFile(run_data.file_info[filename].filename, convert = run_data.file_info[filename].endian)
            elseif gene_file_info.access == "direct"
                FortranFile(run_data.file_info[filename].filename, access="direct", recl = 8)
            else
                error("$(getfield(run_data.file_info[filename], :filename)) requires access specification")
            end
            read_gene_timeslice_binary!(run_data.data[file_id], file_stream, run_data.file_info[filename], only(run_data.timestep), mom_IDs)
            close(file_stream)
        end
    end
    field_IDs = filter(f -> f isa GeneFieldID, run_data.data_fields)
    if !isempty(field_IDs)
        file_id = "field"
        filename = file_id*"_"*run_data.run_ID
        file_stream = if run_data.file_info[filename].access == "sequential"
            FortranFile(run_data.file_info[filename].filename, convert = run_data.file_info[filename].endian)
        elseif gene_file_info.access == "direct"
            FortranFile(run_data.file_info[filename].filename, access="direct", recl = 8)
        else
            error("$(getfield(run_data.file_info[filename], :filename)) requires access specification")
        end
        read_gene_timeslice_binary!(run_data.data[file_id], file_stream, run_data.file_info[filename], only(run_data.timestep), field_IDs)
        close(file_stream)
    end
    return nothing
end
