function computeZAvg()

end

function remapSpectralData(dataIn::Array)
  nkx, nky = size(dataIn)
  T = eltype(dataIn)
  dataOut = Array{T}(undef,nkx-1,nky)
  nkx2 = div(nkx,2)
  # Data will be ordered from (-nkx/2,0) to (nkx/2,nky)
  dataOut[nkx2:nkx-1,:] = dataIn[1:nkx2,:]
  dataOut[1:nkx2-1,:] = dataIn[nkx2+2:nkx,:]
  return dataOut
end
