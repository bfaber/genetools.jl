import Base: filesize, basename
"""
   expand_file_series(string_in::String)

Expands a list of parameters files to analyze continued runs from a string
that can use the following formats:
- a comma separated string, e.g. "1,2,3" => [1,2,3]
- A string with "-" or ":" where a range of values are inserted between the "-"/":"
  - The values inserted can be integers "13-17" => [13,14,15,16,17] or
  - The values can be characters "15a:15g" => ["15a","15b","15c","15d","15e","15f","15g"]

"""
function expand_file_series(string_in::S) where {S <: AbstractString}

    matches = match(r"(.*)([-:,])(.*)", string_in)
    if isnothing(matches)
        return [replace(string_in, "[" => "", "]" => "")]
    elseif length(matches.captures) != 3
        error("Not a valid delimiter for specifying a series range")
    end
    
    dlm = matches.captures[2]
    split_arr = split(string_in, dlm)
    # Functions are involved in reading parameters files
    if dlm == ","
        return strip.(split_arr)

    # Start of functions involved in setting the diagnostic state, such as filling a range
    # of values that can designate the continuation of simulations
    elseif (dlm == "-" || dlm == ":")
      # Determine if the last character is an integer or a character
      lastChar = split_arr[1][length(split_arr[1])]
      temp_type = try parse(Int, lastChar)
        Int
      catch
        # Anything not a an integer is a char
        Char
      end

        if temp_type <: Integer
            left_zeros = match(r"0*", split_arr[1]).match
            # Determine the integer range
            int_low = parse(Int, strip(convert(String, split_arr[1])))
            int_high = parse(Int, string(convert(String, split_arr[2])))
            n_vals = int_high - int_low + 1
            vals = Vector{String}(undef, n_vals)
            for (i, val) in enumerate(int_low:int_high)
                stringI = isempty(left_zeros) ? string(val) : lpad(val, length(split_arr[1]),'0')
                vals[i] = string(stringI)
            end
            return vals
        else
            # Check to make sure the base values for the runs are the same
            base1 = convert(String, split_arr[1][1:end - 1])
            base2 = convert(String, split_arr[2][1:end - 1])
            base1 == base2 ? nothing : throw(ArgumentError("$base2 must be the same value as $base1"))
            # Determine the character range (must be in alphabetical order)
            return string.(base1 .* collect(split_arr[1][end]:split_arr[2][end]))
        end
    else
        throw(ArgumentError("Only '-', ':', and ',' allowed to specify file series"))
    end
end

# Function gets the info on a Gene simulation file, populating the
function get_file_info(parameters::D,
                       file::String;
                      ) where {D <: AbstractDict}
    prec = parameters[:info][:PRECISION]
    endian = parameters[:info][:ENDIANNESS] == "little" ? "little-endian" : "big-endian"
    timesteps = parameters[:general][:comp_type] == "IV" ? parameters[:info][:number_of_computed_time_steps] : nothing
    access="sequential" # Default value
    #access="sequential"
    dataFormat = "binary" # Default value
    dataType = Complex{prec} # Default value
    dataDims = [parameters[:box][:nx0],parameters[:box][:nky0],parameters[:box][:nz0]]
    with_trapping = false
    # handle the different output file cases
    file_type = split(basename(file),"_")[1]
    @debug file_type, file, dataDims
    if file_type == "nrg"
        dataFormat = "text"
        dataType = prec
        istep = parameters[:in_out][:istep_nrg]
        dataDims = [10]
        blockSpec = parameters[:box][:n_spec]
    elseif file_type == "field"
        istep = parameters[:in_out][:istep_field]
        blockSpec = parameters[:info][:n_fields]
    elseif file_type == "mom"
        istep = parameters[:in_out][:istep_mom]
        n_moms = parameters[:info][:n_moms]
        if haskey(parameters[:in_out], :trapdiag)
            if parameters[:in_out][:trapdiag]
                n_moms *= 3
                with_trapping = true
            end
        end
        blockSpec = n_moms
    elseif file_type == "df"
        istep = parameters[:in_out][:istep_dfout]
        dataDims = [parameters[:diag_extd][:num_kx_modes],1,parameters[:box][:nz0],parameters[:box][:nv0],parameters[:box][:nw0]]
        blockSpec = parameters[:box][:n_spec]
    elseif file_type == "triplet"
        access = "direct"
        istep = parameters[:in_out][:istep_triplet]
        dataType = prec
        dataDims = [3*parameters[:diag_extd][:n_tplts]]
        blockSpec = parameters[:box][:n_spec]
    elseif file_type == "omega"
        dataFormat = "text"
        dataType = prec
        istep = parameters[:in_out][:istep_omega]
        dataDims = [3]
        blockSpec = parameters[:box][:n_spec]
    elseif file_type == "eigenvalues"
        dataFormat = "text"
        dataType = prec
        istep = 1
        dataDims = [2]
        blockSpec = parameters[:box][:n_spec]
    else
        return GeneFileInfo{prec}()
    end
    if parameters[:general][:comp_type] == "IV"
        timesteps = parameters[:info][:number_of_computed_time_steps]
        #if records > 1000
    	if timesteps >= 0
            lines = countlines(file)
            if file_type=="nrg"
	        file_steps = div(lines-blockSpec-1, blockSpec+1)
                parameters[:info][:number_of_computed_time_steps] = file_steps * istep
	    elseif dataFormat == "binary"
		file_stream = FortranFile(file, convert = endian)
		file_steps = -1
		CT = prec == Float32 ? ComplexF32 : ComplexF64
		while eof(file_stream) == false
		    read(file_stream,prec)
		    for j in 1:blockSpec
			read(file_stream,CT)
		    end
		    file_steps += 1
		end
		parameters[:info][:number_of_computed_time_steps] = file_steps * istep
	    end
        end
	timesteps = parameters[:info][:number_of_computed_time_steps]
	records = div(timesteps, istep) + 1
        #	records = 1000
        #end
    else
        # Take file, remove path, get eigenvalues file, then find number of eigenvalues computed for this run
        fileDirs = split(file,"/")
        runIDArr = split(fileDirs[end],"_")
        runID = runIDArr[end]
        eval_file = ""
        for i in 2:length(fileDirs)-1
            eval_file = "$(eval_file)/$(fileDirs[i])"
        end
        eval_file = "$(eval_file)/eigenvalues_$runID"
        fStream = open(eval_file,"r")
        fileLines = readlines(fStream)
        close(fStream)
        records = length(fileLines)-1
    end
    record_locs = collect(range(start = 1, step = istep, length = records))
    blockDims = vcat([i for i in dataDims],[blockSpec])
    dataFile = GeneFileInfo{prec}(file, dataType, dataFormat, access,
                                  filesize(file), records, dataDims, 
                                  blockDims, istep, endian,
                                  record_locs, with_trapping)
    return dataFile
end

function get_scan_parameters(nml_file::AbstractString,n_runs::Int)
    file_stream = open(nml_file)
    nml_string = read(file_stream, String)
    close(file_stream)
    par_entries = filter(m -> m.captures[] != "", collect(eachmatch(r"(.*)", nml_string)))
    scan_lines = findall(k -> occursin("scan:", k.captures[]), par_entries)
    scan_dict = Dict()
    for i in scan_lines
        key_str, value_str = Tuple(match(r"(.*)=(.*)", par_entries[i].captures[]).captures)
        key_symbol = Symbol(replace(strip(key_str), " " => "_"))
        value = if occursin(":", strip(value_str))
            _,new_value = split(value_str,":")
            new_value
        else
            sanitize_fortran_input(strip(value_str))
        end
        value = if occursin("#", strip(value))
            new_value,_ = split(value,"#")
            new_value
        else
            sanitize_fortran_input(strip(value_str))
        end
        scan_dict[key_symbol] = Vector{Any}(undef,n_runs)
    end
    
    return scan_dict
end

"""
    read_parameters_file(nml_file::AbstractString)

Reads a GENE parameters file and populates a corresponding `Dict{Symbol,Any}`,
where the `Symbol` key maps directly to the variable label in the parameters file.  Values the type of
the input variable is defined in the global GeneTypeDict `Dict`.
"""
function read_parameters_file(nml_file::AbstractString)
    file_stream = open(nml_file)
    nml_string = read(file_stream, String)
    close(file_stream)

    #nml_string = replace(nml_string, r"\n{2,}" =>"\n")
    # Find locations where the a newline character interrupts data
    newline_array_locs = findall(r"[0-9]\s*\n\s*[0-9]", nml_string)
    while !isempty(newline_array_locs)
        if !isempty(newline_array_locs)
            for r in newline_array_locs
                nml_string = nml_string[1:r[1]]*" "*nml_string[r[end]:end]
            end
        end
        newline_array_locs = findall(r"[0-9]\s*\n\s*[0-9]", nml_string)
    end
    par_entries = filter(m -> m.captures[] != "", collect(eachmatch(r"(.*)", nml_string)))
    namelist_start = findall(k -> occursin("&", k.captures[]), par_entries)
    n_namelists = length(namelist_start)
    namelist_end = findall(k -> k.captures[] == "/", par_entries)
    namelist_ranges = Vector{UnitRange{Int}}(undef, n_namelists)
    map!((s, e) -> range(start = s + 1, stop = e - 1), namelist_ranges, namelist_start, namelist_end)

    parameters_dict = OrderedDict{Symbol, OrderedDict{Symbol, Any}}()
    n_spec = match(r"n_spec\s*=\s*([0-9]*)", par_entries[findfirst(p -> occursin(r"n_spec", p.match), par_entries)].captures[]).captures[]
    @debug n_spec
    species_list = Vector{Symbol}(undef, Meta.parse(n_spec))
    namelist_names = map(i -> replace(first(par_entries[i].captures), "&" => ""), namelist_start)
    # deals with multiple species
    species = findall(n -> n == "species", namelist_names)
    for (j, spec) in enumerate(species)
        for i in namelist_ranges[spec]
            @debug par_entries[i].captures[]
            if occursin(r"name", par_entries[i].match)
                m = match(r"name\s*=\s*(.*)", par_entries[i].match)
                namelist_names[spec] = namelist_names[spec]*"_"*string(Meta.parse(replace(m.captures[], "'" => "")))
                species_list[j] = Symbol(namelist_names[spec])
                break
            end
        end
    end
    for (nml, name) in enumerate(namelist_names)
        dict_name = Symbol(lowercase(name))
        parameters_dict[dict_name] = Dict{Symbol, Any}()
        for i in namelist_ranges[nml]
            key_str, value_str = Tuple(match(r"(.*)=(.*)", par_entries[i].captures[]).captures)
            key_symbol = Symbol(replace(strip(key_str), " " => "_"))
            value = if occursin(" ", strip(value_str))
                new_value = replace(strip(value_str), " " => ", ")
                "["*new_value*"]"
            else
                sanitize_fortran_input(strip(value_str))
            end
            parameters_dict[dict_name][key_symbol] = try
                eval(Meta.parse(value))
            catch
                value
            end
        end
    end

    return parameters_dict, species_list
    
end

"""
    sanitizeFortanInput(s::AbstractString)

Return the correct julia type to be used in Meta.parse
"""
function sanitize_fortran_input(s::AbstractString)
    if lowercase(s) == "f" || lowercase(s) == ".false." || lowercase(s) == ".f."
        return "false"
    elseif lowercase(s) == "t" || lowercase(s) == ".true." || lowercase(s) == ".t."
        return "true"
    elseif occursin("'", s)
        return replace(s, "\'"=>"\"")
    elseif lowercase(s) == "double"
        return "Float64"
    elseif lowercase(s) == "single"
        return "Float32"
    elseif lowercase(s) == "little"
        return "\"little\""
    elseif lowercase(s) == "big"
        return "\"big\""
    else
        return s
    end
end

"""
    get_isteps

Return the value of `istep` for each output file
"""
function get_isteps(diag::DiagControl,
                    run_ID::String,
                    file_list::Union{Vector{String}, Vector{Symbol}, NTuple{N, Symbol}};
                   ) where {N}
    istep_values = Vector{Int}(undef, length(file_list))
    for (i, f) in enumerate(file_list)
        istep_values[i] = diag.parameters[run_ID][:in_out][Symbol(:istep_, f)]
    end
    return istep_values
end
