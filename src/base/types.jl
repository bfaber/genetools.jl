
@enum GeneMomentID::Int32 begin
    dens = 1
    T_par = 2
    T_perp = 3
    q_par = 4
    q_perp = 5
    u_par = 6
end

@enum GeneTrapPassMomentID::Int32 begin
    dens_pass = 1
    T_par_pass = 2
    T_perp_pass = 3
    q_par_pass = 4
    q_perp_pass = 5
    u_par_pass = 6
    dens_trap = 7
    T_par_trap = 8
    T_perp_trap = 9
    q_par_trap = 10
    q_perp_trap = 11
    u_par_trap = 12
    dens_flr = 13
    T_par_flr = 14
    T_perp_flr = 15
    q_par_flr = 16
    q_perp_flr = 17
    u_par_flr = 18
end

@enum GeneFieldID::Int32 begin
    phi = 1
    A_par = 2
    B_par = 3
end

#Only good for one scanned parameter, e.g. scan over ky only
@enum GeneEigenvalueID::Int32 begin
    scan_var = 1
    gamma = 2
    omega = 3
end

GeneIDVector = Union{Vector{GeneEigenvalueID},Vector{GeneFieldID}, Vector{GeneMomentID}, Vector{GeneTrapPassMomentID}, Vector{Enum{Int32}}}


mutable struct GeneData{T}
  times::Array{T}
  data::Array{Complex{T}}
  # Include incomplete constructor
  GeneData{T}() where {T} = new{T}()
  GeneData{T}(a, b) where {T} = new{T}(a, b)
end

@kwdef struct GeneFileInfo{T}
  filename::String = ""
  DataType::Type = Complex{T}
  data_format::String = "binary"
  access::String = "sequential"
  size::Int64 = 0
  records::Int64 = 1
  data_dims::Array{Int} = [1]
  block_dims::Array{Int} = [1]
  istep::Int = 1
  endian::String = "LITTLE"
  rec_locs::Vector{Int} = Vector{Int}()
  with_trapping::Bool = false
end

struct GeneGrid{T}
  kx::Vector{T}
  ky::Vector{T}
  x::Vector{T}
  y::Vector{T}
  z::Vector{T}
  v::Array{T, 2}
  w::Array{T, 2}
  GeneGrid{T}(kx,ky,x,y,z,v,w) where {T} = new{T}(kx,ky,x,y,z,v,w)
  GeneGrid{T}() where {T} = new{T}()
end

mutable struct GeneGeom{T}
  gxx::Vector{T}
  gxy::Vector{T}
  gyy::Vector{T}
  jac::Vector{T}
  modB::Vector{T}
  K1::Vector{T}
  K2::Vector{T}
  dBdz::Vector{T}
  theta::Vector{T}
  GeneGeom{T}() where {T} = new{T}()
end

# The structure that holds the current state of the diagnostic
@kwdef mutable struct DiagControl{T}
    diag_name::String = ""
    data_path::String = ""
    out_path::String = ""
    run_IDs::Vector{String} = Vector{String}()
    run_files::Dict{String, Vector{String}} = Dict()
    file_info::Dict{String, GeneFileInfo} = Dict()
    file_steps::Dict{String, StepRange{Int, Int}} = Dict()
    #data_files::Dict{String, GeneData} = Dict()
    parameters::Dict{String, Dict} = Dict()
    record_times::Dict{String, Vector{Int}} = Dict()
    sim_times::Vector{T} = Vector{T}(undef,0)
    sim_steps::Vector{Int} = Vector{Int}(undef, 0)
    nrg_data::NRG = NRG()
    species_list::Vector{Symbol} = Vector{Symbol}()
    active_species::Vector{Symbol} = Vector{Symbol}()
    geom::GeneGeom = GeneGeom{T}()
    coords::GeneGrid = GeneGrid{T}()
    last_time_step::Bool = false
    start_time::T = zero(T)
    start_step::Int = 1
    end_time::T = zero(T)
    end_step::Int = 1
    sparse_step::Int = 1
    time_average::Bool = true
    diagnostics::Vector{Any} = Vector{Any}()
    scan_diagnostics::Dict{Any,Any} = Dict{Any,Any}()
    scan_parameters::Dict{Symbol,Vector{T}} = Dict{Symbol,Vector{T}}()
    irfft_plan_xyz = plan_irfft(Array{Complex{T}}(undef, 2, 2, 3), 3, [2, 1])
    irfft_plan_xy = plan_irfft(Matrix{Complex{T}}(undef, 2, 2), 3, [2, 1])
    irfft_plan_yz = plan_irfft(Array{Complex{T}}(undef, 2, 2, 2), 3, [2])
    irfft_plan_y = plan_irfft(Matrix{Complex{T}}(undef, 2, 2), 3, [2])
    ifft_plan_xz = plan_ifft(Array{Complex{T}}(undef, 2, 2, 2), [1])
    ifft_plan_x = plan_ifft(Matrix{Complex{T}}(undef, 2, 2), [1])
end

@kwdef mutable struct DiagList
  num_diags::Int = 0
  diagDict::OrderedDict{String,DiagControl} = OrderedDict()
end
