import Base: filesize

mutable struct TripData
  tplts::Array{Int}
  data::Array{Float64}
  time::Array{Float64}
end

function tripletSetup()
  return fileList = ["triplet"]
end

function getTriplets(parDict::Dict)
  ntplts = haskey(parDict[:diag_extd], :n_tplts) ? parDict[:diag_extd][:n_tplts] : error("Not a triplet run");
  tNum = div(length(parDict[:diag_extd][:kx_triplets1]),3);
  tplts = Array{Int}(undef,(3,2,ntplts));
  tCount = 0
  while tCount < ntplts
    for f=1:38
      kxstr = "kx_triplets"*string(f);
      kystr = "ky_triplets"*string(f);
      kxtemp = parDict[kxstr];
      kytemp = parDict[kystr];
      for t in 0:tNum-1
        i = 3*t+1;
        j = i+1;
        k = j+1;
        if kytemp[i] == 0 && kytemp[j] == 0 && kytemp[k] == 0
          nothing;
        else
          tCount += 1;
          tplts[:,:,tCount] = hcat(kxtemp[i:k],kytemp[i:k]);
        end
      end
    end
  end
  return tplts;
end

function readTripletData(diag::DiagControl,runID::String,tripType::String="es")
  # Get the file info
  tripData = GeneData()
  key = "triplet_"*tripType*"_"*runID
  tripInfo = diag.dataFiles[key]
  ntplts = diag.parDicts[runID]["n_tplts"][1];
  T = tripInfo.dataType
  blockSize = tripInfo.blockDims[length(tripInfo.blockDims)]
  times = Array{Float64}(undef,tripInfo.records);
  dataArr = Array{T}(undef,Tuple(vcat(tripInfo.blockDims,tripInfo.records)))
  recLocs = hcat(map(i->i,range(1,stop=tripInfo.records)),map(i->i,range(1,stop=tripInfo.records)))
  fieldLocs = map(i->i,range(1,stop=blockSize))
  data = Array{Float64}(undef,(3,ntplts,blockSize,tripInfo.records))
  times = readGeneBinary!(tripInfo,dataArr,recLocs,fieldLocs)
  for i=1:tripInfo.records
    for k=1:blockSize
      for j = 1:ntplts
        data[:,j,k,i] = dataArr[(j-1)*3+1:3*j,k,i]
      end
    end
  end
  close(tFile); 
  return times, data;
end

function findTriplets(tData::GeneData, tplts::Array{Int,3}, tripKey::Tuple)
  nTplts = size(tplts,3)
  #tripLocs = Array{Int}(undef,(1,1))
  tripLocs = Array{Int}(undef,0)
  for i = 1:nTplts
    for j = 1:3
      if Tuple(tplts[j,:,i]) == tripKey
         push!(tripLocs,i)
      end
    end
  end
  subTplts = tplts[:,:,tripLocs]
  subData = tData.data[:,tripLocs,:,:]
  nSubTplts = length(tripLocs)
  for i = 1:nSubTplts
    for j = 1:3
      if Tuple(subTplts[j,:,i]) == tripKey
        if j > 1
          tt = subTplts[1,:,i]
          td = subData[1,i,:,:]
          subTplts[1,:,i] = subTplts[j,:,i]
          subData[1,i,:,:] = subData[j,i,:,:]
          subTplts[j,:,i] = tt
          subData[j,i,:,:] = td
        end
      end
    end
  end
  newData = GeneData()
  newData.times = tData.times
  newData.data = subData
  return newData, subTplts
end

function getTripletPlotData(diag::DiagControl, tdata::GeneData, tplts::Array{Int,3})
  kx = diag.coords.x
  nkx = length(kx)
  ky = diag.coords.y
  nky = length(ky)
  ntplts = size(tplts,3)
  pTplts = Array{Int}(undef,size(tplts))
  px = Array{Float64}(undef,3,ntplts)
  py = Array{Float64}(undef,3,ntplts)
  for i = 1:ntplts
    for j = 1:3
      pTplts[j,2,i] = tplts[j,2,i] + 1
      if tplts[j,1,i] < 0
        pTplts[j,1,i] = nkx + tplts[j,1,i] + 1
      else
        pTplts[j,1,i] = tplts[j,1,i] + 1
      end
      px[j,i] = kx[pTplts[j,1,i]]
      py[j,i] = ky[pTplts[j,2,i]]
    end
  end
  p = sortperm(px[2,:])
  px = px[:,p]
  py = py[:,p]
  meanData = mean(tdata.data[:,:,1,:],dims=3)
  data = meanData[:,p]

  writeArr = Array{Float64,2}(undef,ntplts,5)
  for i = 1:ntplts
    writeArr[i,1] = px[2,i]
    writeArr[i,2] = py[2,i]
    for j = 1:3
      writeArr[i,2+j] = data[j,i]
    end
  end
  display(writeArr)

  filename = joinpath(diag.dataPath,"triplet_plot_data.dat")
  writedlm(filename,writeArr)


  return px, py, data
end

function computeTkk(k1::Tuple{Int,2}, k2::Tuple{Int,2}, k3::Tuple{Int,2}, m1::Array{ComplexF64,6}, m2::Array{ComplexF64,6}, m3::Array{ComplexF64,6})

end

function plotTripletSeries(data::GeneData)

end

function diagTriplet(file::String,parDict::Dict)
  # Initialize the triplets
  triplets = TripData(Array{Int}(undef),Array{Float64}(undef),Array{Float64}(undef));
  triplets.tplts = getTriplets(parDict);
  triplets.time,triplets.data = readTripletData(file,parDict);
  
  # Plot the triplets as a time series
  tripletTimeSeries = TimeSeries(triplets.time,triplets.data);
  return nothing;
end
