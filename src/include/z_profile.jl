# Plots absolute value of field given kx,ky indices. If indices are not given, a spatial average is taken.
# Some restructuring needs to be done to easily pass kx, ky values along;
# it seems much of this was written with nonlinear runs in mind.
# norm normalizes the data to the maximum value
# non_zonal zeros out the ky = 0 data
function ZProfile(diag::DiagControl{T},
                  var_list::Union{Symbol, Vector{Symbol}, NTuple{N, Symbol}} = (:phi,);
                  kx_index::Union{Int, Vector{Int}} = -1,
                  ky_index::Union{Int, Vector{Int}} = -1,
                  norm::Bool = true,
                  non_zonal::Bool = false
                 ) where {T, N}
    vars = Tuple(unique(var_list isa Vector ? Tuple(var_list...) : Tuple(var_list)))
    nz = length(diag.coords.z)
    n_spec = length(diag.active_species)

    kx = kx_index isa Vector ? kx_index : [kx_index]
    ky = ky_index isa Vector ? ky_index : [ky_index]

    n_pol = haskey(diag.parameters[diag.run_IDs[1]][:geometry] ,:n_pol) ?  diag.parameters[diag.run_IDs[1]][:geometry][:n_pol] : 1

    dz = 2*n_pol/nz
    z = [-n_pol:dz:n_pol-dz;]
    nx = length(diag.coords.x)
    ny = length(diag.coords.y)
    
    data = Array{Complex{T}, 3}(undef, nx, ny, nz)
    plot_data = eval(Expr(:call, :ComponentArray, map(i -> Expr(:kw, i, zeros(T, length(kx), length(ky), nz, n_spec)), vars)...))
    return ZProfile{T, Tuple(diag.active_species), (:field, :mom)}(data, plot_data, vars, norm, non_zonal, kx, ky, z)
end

function ZProfile()
    return ZProfile{Float64, (:i, :e), (:field, :mom)}(ComponentArray(phi = Vector{Float64}()), (GeneTools.phi,), (true, false), (true, false))
end


function run_diag!(z_prof_diag::ZProfile{T, S, F},
                   diag::DiagControl{T},
                   timeslice_data::GeneTimesliceData{T, S, F},
                  ) where {T, S, F}
    z_profile!(z_prof_diag, diag, timeslice_data)
end

function z_profile!(z_prof_diag::ZProfile{T, S, F},
                     diag::DiagControl{T},
                     timeslice_data::GeneTimesliceData{T, S, F}
                    ) where {T, S, F}
    data_view = abs.(view(z_prof_diag.data, :, :, :))
    data_view_kx_ky = Array{T}(undef,length(z_prof_diag.kx),length(z_prof_diag.ky),length(z_prof_diag.z))
    for (j, s) in enumerate(species(z_prof_diag))
        for var in z_prof_diag.var_list
            data_view = if var in [:phi, :A_par, :B_par]
                filename = "field"
                abs.(view(timeslice_data.data[filename][var], :, :, :))
            else
                filename = "mom_"*string(s)
                abs.(view(timeslice_data.data[filename][var], :, :, :))
            end
	    if z_prof_diag.non_zonal
                data_view[:,1,:] .*= 0
            end
            if z_prof_diag.kx == [-1] && z_prof_diag.kx == [-1] 
		data_view_kx_ky = sum(sum(data_view,dims=2),dims=1)./(size(data_view)[1]*size(data_view)[2])
            elseif z_prof_diag.kx == [-1] 
                data_view = sum(data_view,dims=1)./size(data_view)[1]
		for ky in eachindex(z_prof_diag.ky)
                    data_view_kx_ky[:,ky,:] .= data_view[:, z_prof_diag.ky[ky], :]
                end
            elseif z_prof_diag.ky == [-1] 
                data_view = sum(data_view,dims=2)./size(data_view)[2]
                for kx in eachindex(z_prof_diag.kx)
                    data_view_kx_ky[kx,:,:] .= data_view[z_prof_diag.kx[kx], :, :]
                end
            else
                for kx in eachindex(z_prof_diag.kx)
                    for ky in eachindex(z_prof_diag.ky)
                        data_view_kx_ky[kx,ky,:] .= data_view[z_prof_diag.kx[kx], z_prof_diag.ky[ky], :]
                    end
                end
            end
            add_plot_data!(z_prof_diag, var, [:, :, :, j], z_profile(diag, data_view_kx_ky, z_prof_diag.norm))
        end
    end
end

function z_profile(diag::DiagControl{T},
                    data::AbstractArray{T},
                    norm::Bool) where {T}
    for x in axes(data,1)
        for y in axes(data,2)
            data[x,y,:] ./= ((1+norm*(maximum(abs.(data[x,y,:]))-1)))
        end
    end
    return data
end

function get_fields_and_moments(z_prof_diag::ZProfile{T, S, F}) where {T, S, F}
    return [z_prof_diag.var_list...]
end

function get_diag_name(z_prof_diag::ZProfile{T, S, F};) where {T, S, F}
    return "z_profile"
end

function save_plot_data(z_prof_diag::ZProfile{T, S, F},
                        spec_index::Int,
                        fid;
                       ) where {T, S, F}
    fid["z"] = z_prof_diag.z
    fid["kx"] = z_prof_diag.kx
    fid["ky"] = z_prof_diag.ky

    for var in keys(z_prof_diag.plot_data)
        string_var = String(var)
        fid[string_var] = z_prof_diag.plot_data[var][:,:,:,spec_index]
    end
    return nothing
end

function save_plot_data(z_prof_diag::ZProfile{T, S, F},
                        spec_index::Int,
                        fid,
                        run_ID::String;
                       ) where {T, S, F}
    create_group(fid, run_ID)
    for var in keys(z_prof_diag.plot_data)
        string_var = String(var)
        fid["$run_ID/z"] = z_prof_diag.z
        fid["$run_ID/kx"] = z_prof_diag.kx
        fid["$run_ID/ky"] = z_prof_diag.ky
        fid["$run_ID/$string_var"] = z_prof_diag.plot_data[var][:,:,:,spec_index]
    end
    return nothing
end
