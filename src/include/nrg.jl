function NRG(diag::DiagControl{T},
             nrg_list::Union{Symbol, Vector{Symbol}, NTuple{N, Symbol}} = (:n, :u_par, :T_par, :T_perp, :Γ_es, :Γ_em, :Q_es, :Q_em, :Π_es, :Π_em),
            ) where {T, N}

    which_files = Vector{Bool}(undef, length(diag.run_IDs))
    for (i, run) in enumerate(diag.run_IDs)
        which_files[i] = ((diag.start_step in diag.file_steps[run]) || 
                          (diag.end_step in diag.file_steps[run]) ||
                          issubset(diag.file_steps[run], diag.start_step:diag.end_step))
    end

    n_timesteps = 0
    for (i, run) in enumerate(diag.run_IDs)
        if which_files[i]
       	    start_step = max(diag.start_step, first(diag.file_steps[run])) - first(diag.file_steps[run]) + 1
            end_step = min(diag.end_step, last(diag.file_steps[run])) - first(diag.file_steps[run]) + 1
            n_timesteps += div(end_step-start_step,step(diag.file_steps[run]))#length(max(first(diag.file_steps[run]), diag.start_step):step(diag.file_steps[run]):min(last(diag.file_steps[run]), diag.end_step))
	end
    end
    spec_list = Tuple(diag.active_species)
    n_spec = length(spec_list)
    axis_args = map(i -> Expr(:kw, spec_list[i], i), 1:n_spec)
    
    ax = eval(Expr(:call, :Axis, axis_args...))
    plot_data = ComponentArray(map(i -> zeros(T, length(nrg_list), n_timesteps), 1:n_spec), ax)
    times = Vector{T}(undef, n_timesteps)
    return NRG{T, spec_list, (:nrg,)}(plot_data, times, nrg_list, which_files)
    
end

function NRG()
    return NRG{Float64, (:ions, :electrons), (:nrg,)}(ComponentArray(ions = Vector{Float64}(), electrons = Vector{Float64}()),
                                                     Vector{Float64}(), (:n, :u_par, :T_par, :T_perp, :Γ_es, :Γ_em, :Q_es, :Q_em, :Π_es, :Π_em),
                                                     Vector{Bool}())
end

function run_diag!(diag::DiagControl{T},
                  nrg_diag::NRG{T, S, F}
                  ) where {T, S, F}
    which_fields = findall(i -> i in [nrg_diag.nrg_list...], [:n, :u_par, :T_par, :T_perp, :Γ_es, :Γ_em, :Q_es, :Q_em, :Π_es, :Π_em])
    offset = 1
    
    for (i, f) in enumerate(nrg_diag.which_files)
        if f
            run = diag.run_IDs[i]
            start_step = max(diag.start_step, first(diag.file_steps[run])) - first(diag.file_steps[run]) + 1
            end_step = min(diag.end_step, last(diag.file_steps[run])) - first(diag.file_steps[run]) + 1
            step_locs = div.(collect(start_step:step(diag.file_steps[run]):end_step - 1) , step(diag.file_steps[run])) .+ 1
	    n_steps = length(step_locs)
	    data = Matrix{T}(undef, first(diag.file_info["nrg_"*run].block_dims),  length(diag.active_species) * length(step_locs))
            times = read_gene_nrg_file!(data, diag.file_info["nrg_"*run], step_locs, [1])
            copyto!(nrg_diag.times, offset, times, 1, n_steps)
            for (i, s) in enumerate(species(nrg_diag))
                add_plot_data!(nrg_diag, s, [which_fields, offset:offset+n_steps-1], data[which_fields, i:length(species(nrg_diag)):end])
            end
            offset += n_steps
        end
    end
end

