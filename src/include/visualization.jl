function setup_figure end

function plot_diagnostic end

function plot_scan_diagnostic end

#Extra plotting routines to make more paper-relavent plots
#by accessing the .h5 data files written by save_plot_data 
# or save_scan_plot_data
function compare_eigenvalues end

function plot_mean_cross_phases end

function plot_linear_and_nonlinear_cross_phases end

function compare_geometry end

function compare_z_profile end

function compare_z_profile_and_geometry end

function compare_flux_spectra end
