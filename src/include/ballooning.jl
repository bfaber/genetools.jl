
function Ballooning(diag::DiagControl{T},
                    var_list::Union{Symbol, Vector{Symbol}, NTuple{N, Symbol}} = (:phi,),
                    norm::Bool = true,
                   ) where {T, N}
    vars = Tuple(unique(var_list isa Vector ? Tuple(var_list...) : Tuple(var_list)))
    nz = length(diag.coords.z)
    n_spec = length(diag.active_species)
    
    n_pol = diag.parameters[diag.run_IDs[1]][:geometry][:n_pol]
    nx = diag.parameters[diag.run_IDs[1]][:box][:nx0]

    max_theta = nx*n_pol  
    dz = 2*n_pol/nz
    z_ext = [-max_theta:dz:max_theta-dz;]
    nx = length(diag.coords.x)
    ny = length(diag.coords.y)
    nz_ext = length(z_ext)
    
    data = Array{Complex{T}, 3}(undef, nx, ny, nz)
    plot_data = eval(Expr(:call, :ComponentArray, map(i -> Expr(:kw, i, zeros(Complex{T}, nz_ext, n_spec)), vars)...))
    return Ballooning{T, Tuple(diag.active_species), (:field, :mom)}(data, plot_data, vars, norm, z_ext)
end

function Ballooning()
    return Ballooning{Float64, (:i, :e), (:field, :mom)}(ComponentArray(phi = Vector{Float64}()), (GeneTools.phi,), (true, false), (true, false))
end

function extend_time_slice_data(diag::DiagControl{T},
                                time_slice_data::AbstractArray;
                                ) where {T}
    nx,ny,nz = size(time_slice_data)
    nx_even = iseven(nx)
    n_domains = nx-nx_even
    nx_mod = div(n_domains,2)
    run = diag.run_IDs[1]
    n_pol = haskey(diag.parameters[run][:geometry],"n_pol") ? diag.parameters[run][:geometry] : 1
    shat = diag.parameters[run][:geometry][:shat]
    kymin = diag.parameters[run][:box][:kymin]
    Lx = diag.parameters[run][:info][:lx]
    sign_Ip_CW = haskey(diag.parameters[run][:geometry],"sign_Ip_CW") ? diag.parameters[run][:geometry][:sign_Ip_CW] : 1
    sign_Bt_CW = haskey(diag.parameters[run][:geometry],"sign_Bt_CW") ? diag.parameters[run][:geometry][:sign_Bt_CW] : 1
    
    time_slice_data_ext = Array{Complex{Float64}}(undef,ny,nz*n_domains)
    
    ky_inds = 1
    sign_nexc = sign(shat)*sign_Ip_CW*sign_Bt_CW
    num_2pinpol_from_tube = [-nx_mod:nx_mod;]
    nexc = sign_nexc*(1 - diag.parameters[run][:box][:adapt_lx]*(1-abs(shat)*kymin*Lx*ky_inds*n_pol))
    kx_prime_idx = vcat([nx-nx_mod+1:nx;],[1:nx_mod+1;])
    if sign(shat) == -1
        kx_prime_idx = reverse(kx_prime_idx)
    end

    phasefac = (-1.0+im*0.0)^nexc
    for j=1:n_domains
        time_slice_data_ext[:,(j-1)*nz+1:j*nz] = phasefac^abs(num_2pinpol_from_tube[j]) .* time_slice_data[kx_prime_idx[j],:,:]
    end

    return time_slice_data_ext
end

function run_diag!(ball_diag::Ballooning{T, S, F},
                   diag::DiagControl{T},
                   timeslice_data::GeneTimesliceData{T, S, F},
                  ) where {T, S, F}
    ballooning!(ball_diag, diag, timeslice_data)
end

function ballooning!(ball_diag::Ballooning{T, S, F},
                     diag::DiagControl{T},
                     timeslice_data::GeneTimesliceData{T, S, F}
                    ) where {T, S, F}
    data_view = view(ball_diag.data, :, :, :)
    for (j, s) in enumerate(species(ball_diag))
        for var in ball_diag.var_list
            data_view = if var in [:phi, :A_par, :B_par]
                filename = "field"
                view(timeslice_data.data[filename][var], :, :, :)
            else
                filename = "mom_"*string(s)
                view(timeslice_data.data[filename][var], :, :, :)
            end
            add_plot_data!(ball_diag, var, [:, j], ballooning(diag, data_view, ball_diag.norm))
        end
    end
end

function ballooning(diag::DiagControl{T},
                    data::AbstractArray{Complex{T}},
                    norm::Bool) where {T}
    ext_data = extend_time_slice_data(diag, data)
    ny,nz = size(ext_data)
    data_sum_ky = reshape(sum(ext_data,dims=1)/ny,nz)
    println(maximum(abs.(data_sum_ky)))
    return data_sum_ky./((1+norm*(maximum(abs.(data_sum_ky))-1)))
end

function get_fields_and_moments(ball_diag::Ballooning{T, S, F}) where {T, S, F}
    return [ball_diag.var_list...]
end

function get_diag_name(ball_diag::Ballooning{T, S, F};) where {T, S, F}
    return "ballooning"
end

function save_plot_data(ball_diag::Ballooning{T, S, F},
                        spec_index::Int,
                        fid;
                       ) where {T, S, F}
    fid["z"] = ball_diag.z_ext
    for var in keys(ball_diag.plot_data)
        string_var = String(var)
        fid[string_var] = ball_diag.plot_data[var][:,spec_index]
    end
    return nothing
end

function save_plot_data(ball_diag::Ballooning{T, S, F},
                        spec_index::Int,
                        fid,
                        run_ID::String;
                       ) where {T, S, F}
    create_group(fid, run_ID)
    for var in keys(ball_diag.plot_data)
        string_var = String(var)
        fid["$run_ID/z"] = ball_diag.z_ext
        fid["$run_ID/$string_var"] = ball_diag.plot_data[var][:,spec_index]
    end
    return nothing
end