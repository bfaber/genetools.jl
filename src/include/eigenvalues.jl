function Eigenvalues(diag::DiagControl{T},
                    ) where {T}

    vars = Vector([:scan_var,:gamma,:omega])
    ax_args = map(i -> Expr(:kw, vars[i], i), eachindex(vars))
    ax = eval(Expr(:call, :(ComponentArrays.Axis), ax_args...))
    plot_data = eval(Expr(:call, :ComponentArray, map(i -> Expr(:kw, i, zeros(T,1)), vars)...), ) 
    return Eigenvalues{T, (:none,), (:omega,)}(plot_data, vars)
end

function Eigenvalues()
    return Eigenvalues{Float64, (:none,), (:omega,)}(ComponentArray(gamma = Vector{Float64}, omega = Vector{Float64}), (GeneTools.gamma,GeneTools.omega))
end

function run_diag!(eigenvalues_diag::Eigenvalues{T, S, F},
                   diag::DiagControl{T},
                   run::String
                  ) where {T, S, F}
    eigenvalues!(eigenvalues_diag,diag,run)
end

function eigenvalues!(eigenvalues_diag::Eigenvalues{T, S, F},
                      diag::DiagControl{T},
                      run::String
                     ) where {T, S, F}
    for var in eigenvalues_diag.var_list
        add_plot_data!(eigenvalues_diag, var, [:],eigenvalues!(diag.file_info["omega_" * run],var))
    end
end

function eigenvalues!(gene_file::GeneFileInfo{T},var::Symbol) where {T}
    file = isfile(gene_file.filename) ? gene_file.filename : throw(ArgumentError("File $(gene_file.filename) does not exist"))
    file_stream = open(file, "r")
    file_lines = readlines(file_stream)
    close(file_stream)
    scan_val, gamma, omega = T.(eval.(Meta.parse.(strip.(split(strip(file_lines[1]),"   ")))))
    eigenvalue_dict = Dict(:scan_var => scan_val, :gamma => gamma, :omega => omega)
    return eigenvalue_dict[var]
end

function get_diag_name(eigenvalues_diag::Eigenvalues{T, S, F};) where {T, S, F}
    return "eigenvalues"
end

function get_fields_and_moments(eigenvalues_diag::Eigenvalues{T, S, F}) where {T, S, F}
    return [eigenvalues_diag.var_list...]
end

function save_plot_data(eigenvalues_diag::Eigenvalues{T, S, F},
                        spec_index::Int,
                        fid;
                       ) where {T, S, F}
    for var in keys(eigenvalues_diag.var_list)
        string_var = String(var)
        create_group(fid, string_var)
        fid[string_var] = eigenvalues_diag.plot_data[var][:]
    end
    return nothing
end

function save_plot_data(eigenvalues_diag::Eigenvalues{T, S, F},
                        spec_index::Int,
                        fid,
                        run_ID::String;
                       ) where {T, S, F}
    create_group(fid, run_ID)
    for var in keys(eigenvalues_diag.plot_data)
        string_var = String(var)
        fid["$run_ID/$string_var"] = eigenvalues_diag.plot_data[var][:]
    end
    return nothing
end
