
function FluxSpectra(diag::DiagControl{T},
                     flux_list::Union{Symbol, Vector{Symbol}, NTuple{N, Symbol}} = (:Γ_es, :Q_es, :Γ_em, :Q_em),
                     kx::Bool = true,
                     ky::Bool = true;
                     which_kx::Union{Vector{S}, Vector{I}} = [-1],
                     which_ky::Union{Vector{S}, Vector{I}} = [-1],
                     which_z::Union{Vector{S}, Vector{I}} = [-1],
                    ) where {T, N, S <: AbstractFloat, I <: Integer}
    if diag.parameters[diag.run_IDs[1]][:general][:beta] == 0.0
        flux_list = filter(x->!occursin("m",String(x)),flux_list)
    end
    fluxes = flux_list isa Vector ? Tuple(flux_list...) : Tuple(flux_list)
    n_fluxes = length(fluxes)
    n_spec = length(diag.active_species)
    nk = 0
    kx_tuple = if kx
        nk += 1
        if which_kx != [-1]
            n_kx = length(which_kx)
            if which_kx isa Vector{Int}
                kx_range = copy(diag.coords.kx[which_kx])
                kx_inds = copy(which_kx)
            else
                kx_inds = map(i -> findfirst(j -> isapprox(i, j, atol = eps(T)), diag.coords.kx), which_kx)
                kx_inds isa Vector{Int} || error("which_kx does not have compatilble entries")
                kx_range = copy(which_kx)
            end
            (true, kx_range, kx_inds)
        else
            n_kx = div(length(diag.coords.kx), 2) + 1
            kx_range = copy(diag.coords.kx[1:n_kx])
            kx_inds = collect(1:n_kx)
            (true, kx_range, kx_inds)
        end
    else
        n_kx = 0
        (false, Vector{S}(), Vector{I}())
    end
    
    ky_tuple = if ky
        nk += 1
        if which_ky != [-1]
            n_ky = length(which_ky)
            if which_ky isa Vector{Int}
                ky_range = copy(diag.coords.ky[which_ky])
                ky_inds = copy(which_ky)
            else
                ky_inds = map(i -> findfirst(j -> isapprox(i, j, atol = eps(T)), diag.coords.ky), which_ky)
                ky_inds isa Vector{Int} || error("which_ky does not have compatilble entries")
                ky_range = copy(which_ky)
            end
            (true, ky_range, ky_inds) 
        else
            n_ky = length(diag.coords.ky)
            ky_range = copy(diag.coords.ky)
            ky_inds = collect(1:n_ky)
            (true, ky_range, ky_inds)
        end
    else
        n_ku = 0
        (false, Vector{S}(), Vector{I}())
    end
    
    z_tuple = if which_z == [-1]
        z = collect(range(start = 1, stop = length(diag.coords.z)))
        nz = length(z)
        nz_plot = 1
        (true, z)
    else
        nz = length(which_z)
        nz_plot = nz
        (false, which_z)
    end 

    data = ComponentArray(kx = Array{T, 3}(undef, n_kx, nz, n_fluxes), ky = Array{T, 3}(undef, n_ky, nz, n_fluxes))
    plot_data = ComponentArray(kx = zeros(T, n_kx, nz_plot, n_fluxes, n_spec), ky = zeros(T, n_ky, nz_plot, n_fluxes, n_spec))
    return FluxSpectra{T, Tuple(diag.active_species), (:field, :mom)}(data, plot_data, flux_list, kx_tuple, ky_tuple, z_tuple,
                                                                      zeros(Complex{T}, length(diag.coords.kx), length(diag.coords.ky), length(diag.coords.z)),
                                                                      zeros(Complex{T}, length(diag.coords.kx), length(diag.coords.ky), length(diag.coords.z)),
                                                                      zeros(Complex{T}, length(diag.coords.kx), length(diag.coords.ky), length(diag.coords.z)),
                                                                      zeros(Complex{T}, length(diag.coords.kx), length(diag.coords.ky), length(diag.coords.z)))
end

function FluxSpectra()
    return FluxSpectra{Float64, (:ions, :electrons), (:field, :mom)}(ComponentArray(kx = Vector{Float64}(), ky = Vector{Float64}()),
                                                                    ComponentArray(kx = Vector{Float64}(), ky = Vector{Float64}()),
                                                                    (:Γ_es, :Q_es, :Γ_em, :Q_em),
                                                                    (true, Vector{Float64}(), Vector{Int}()),
                                                                    (true, Vector{Float64}(), Vector{Int}()),
                                                                    (true, Vector{Int}()),
                                                                    Vector{ComplexF64}(), Vector{ComplexF64}(),
                                                                    Vector{ComplexF64}(), Vector{ComplexF64}())
end

function run_diag!(flux_diag::FluxSpectra{T, S, F},
                   diag::DiagControl{T},
                   timeslice_data::GeneTimesliceData{T, S, F},
                  ) where {T, S, F}
    flux_spectrum!(flux_diag, diag, timeslice_data)
end

function flux_spectrum!(flux_diag::FluxSpectra{T, S, F},
                        diag::DiagControl{T},
                        timeslice_data::GeneTimesliceData{T, S, F},
                       ) where {T, S, F}
    

    if (:Γ_es in flux_diag.flux_list) || (:Q_es in flux_diag.flux_list) || (:P_es in flux_diag.flux_list)
        ϕ = timeslice_data.data["field"][:phi]
        ExB_velocity_x!(flux_diag.u_ExB_x, ϕ, diag)
    end
    if (:Γ_em in flux_diag.flux_list) || (:Q_em in flux_diag.flux_list) || (:P_em in flux_diag.flux_list)
        A_par = timeslice_data.data["field"][:A_par]
        B_x!(flux_diag.B_x, A_par, diag)
    end
    with_kx = flux_diag.kx
    with_ky = flux_diag.ky
    z_inds = flux_diag.z[1] ? [-1] : flux_diag.z[2]
    jacobian = diag.geom.jac / sum(diag.geom.jac) * length(diag.geom.jac)
    A_view = view(flux_diag.A, :, :, :)
    B_view = view(flux_diag.B, :, :, :)
    for (j, s) in enumerate(species(flux_diag))
        filename = "mom_"*string(s)
        for (i, flux) in enumerate(flux_diag.flux_list)
            if flux === :Γ_es
                A_view .= view(timeslice_data.data[filename][:dens], :, :, :)
                B_view .= view(flux_diag.u_ExB_x, :, :, :)
            elseif flux === :Q_es
                A_view .= (1.5 * view(timeslice_data.data[filename][:dens], :, :, :)
                           .+ 0.5 * view(timeslice_data.data[filename][:T_par], :, :, :)
                           .+ view(timeslice_data.data[filename][:T_perp], :, :, :))
                B_view .= view(flux_diag.u_ExB_x, :, :, :)
            elseif flux === :Γ_em
                A_view .= view(timeslice_data.data[filename][:u_par], :, :, :)
                B_view .= view(flux_diag.B_x, :, :, :)
            elseif flux === :Q_em
                A_view .= (view(timeslice_data.data[filename][:q_par], :, :, :)
                           .+ view(timeslice_data.data[filename][:q_perp], :, :, :))
                B_view .= view(flux_diag.B_x, :, :, :)
            elseif flux === :P_es
                A_view .= view(timeslice_data.data[filename][:u_par], :, :, :)
                B_view .= view(flux_diag.u_ExB_x, :, :, :)
            elseif flux === :P_em
                A_view .= (1.5 * view(timeslice_data.data[filename][:dens], :, :, :)
                           .+ 0.5 * view(timeslice_data.data[filename][:T_par], :, :, :))
                B_view .= view(flux_diag.B_x, :, :, :)
            else
                throw(ArgumentError("$(flux) is not a valid flux option"))
            end
            if with_kx[1]
                plot_data_view = view(flux_diag.plot_data, :kx) 
                data_view = view(flux_diag.data, :kx)
                add_plot_data!(flux_diag, :kx, [:, :, i, j],
                               flux_spectrum_kx!(view(data_view, :, :, j), A_view, B_view, with_kx[3], z_inds, jacobian))
                #add_to_diagnostic!(flux_diag, [:kx, i, j], data_view)
            end
            if with_ky[1]
                plot_data_view = view(flux_diag.plot_data, :ky) 
                data_view = view(flux_diag.data, :ky)
                add_plot_data!(flux_diag, :ky, [:, :, i, j],
                               flux_spectrum_ky!(view(data_view, :, :, j), A_view, B_view, with_ky[3], z_inds, jacobian))
                #add_to_diagnostic!(flux_diag, [:ky, i, j], data_view)
            end
        end
    end
    
end

function flux_spectrum_kx!(data::AbstractArray{T},
                           A::AbstractArray{Complex{T}},
                           B::AbstractArray{Complex{T}},
                           kx_inds::AbstractVector{Int},
                           z_inds::AbstractVector{Int},
                           jacobian::AbstractVector{T},
                          ) where {T}
    which_z = z_inds == [-1] ? collect(axes(data, 2)) : z_inds
    flux_spectrum_kx!(data, A, B, kx_inds, which_z)
    if z_inds == [-1]
        res = Vector{T}(undef, size(data, 1))
        for i in axes(data, 1)
            data_view = view(data, i, :)
            res[i] = sum(data_view .* jacobian)
        end
        return res / length(jacobian)
    else
        return data
    end
end

function flux_spectrum_kx!(res::AbstractArray{T},
                           A::AbstractArray{Complex{T}},
                           B::AbstractArray{Complex{T}},
                           kx_inds::AbstractVector{Int},
                           z_inds::AbstractVector{Int}
                          ) where {T}
    size(A) == size(B) || throw(DimensionMismatch("Incorrect dimensions between A ($(size(A)) and B ($(size(B)))"))
    nkx, nky, nz = size(A)
    #kx_inds = which_kx == [-1] ? collect(1:nkx2) : which_kx
    #z_inds = which_z == [-1] ? collect(1:nz) : which_z
    #res = Array{T}(undef, length(kx_inds), length(z_inds))
    #temp = similar(res)
    @inbounds for (z, z_ind) in enumerate(z_inds)
        @views for (i, kx_ind) in enumerate(kx_inds)
            res[i, z] = flux_spectrum_kx(kx_ind, A[:, :, z_ind], B[:, :, z_ind])
        end
    end
    repeated = findall(i -> i == 1 || i == div(nkx, 2) + 1, kx_inds)
    if !isempty(repeated)
        res_view = view(res, repeated, :)
        res_view[:, :] .*= 0.5
    end

    return nothing
end

@inline function flux_spectrum_kx0(A::AbstractArray{Complex{T}},
                                   B::AbstractArray{Complex{T}},
                                  ) where {T}
    nky = size(A, 2)
    return real(A[1, 1] * B[1, 1]) + 
           2 * real(sum(conj.(view(A, 1, 2:nky)) .* view(B, 1, 2:nky)))
end

@inline function flux_spectrum_kx2(A::AbstractArray{Complex{T}},
                                   B::AbstractArray{Complex{T}},
                                  ) where {T}
    nkx, nky = size(A)
    nkx2 = div(nkx, 2) + 1
    return real(A[nkx2, 1] * B[nkx2, 1]) + 
           2 * real(sum(conj.(view(A, nkx2, 2:nky)) .* view(B, nkx2, 2:nky))) 
end

@inline function flux_spectrum_kx(kx_index::Int,
                                  A::AbstractArray{Complex{T}},
                                  B::AbstractArray{Complex{T}},
                                 ) where {T}
    nkx, nky = size(A)
    n_kx_index = ((nkx - kx_index + 1) % nkx) + 1
    res = conj(A[kx_index, 1]) * B[kx_index, 1]
    @inbounds for i in 2:nky
        res +=  conj(A[kx_index, i]) * B[kx_index, i] + conj(A[n_kx_index, i]) * B[n_kx_index, i]
    end
    return 2 * real(res)
    #return 2 * real(conj(A[kx_index, 1]) * B[kx_index, 1] .+
    #                sum(conj.(view(A, kx_index, 2:nky)) .* view(B, kx_index, 2:nky) .+
    #                    conj.(view(A, n_kx_index, 2:nky)) .* view(B, n_kx_index, 2:nky)))
end          

@inline function flux_spectrum_kx(A_pos::AbstractVector{Complex{T}},
                                  A_neg::AbstractVector{Complex{T}},
                                  B_pos::AbstractVector{Complex{T}},
                                  B_neg::AbstractVector{Complex{T}},
                                 ) where {T}
    nky = length(A_pos)
    return 2 * real(conj(A_pos[1]) * B_pos[1] .+
                    sum(conj.(view(A_pos, 2:nky)) .* view(B_pos, 2:nky) .+
                        conj.(view(A_neg, 2:nky)) .* view(B_neg, 2:nky)))
end                           


function flux_spectrum_ky!(data::AbstractArray{T},
                           A::AbstractArray{Complex{T}},
                           B::AbstractArray{Complex{T}},
                           ky_inds::AbstractVector{Int},
                           z_inds::AbstractVector{Int},
                           jacobian::AbstractVector{T},
                          ) where {T}
    which_z = z_inds == [-1] ? collect(axes(data, 2)) : z_inds
    flux_spectrum_ky!(data, A, B, ky_inds, which_z)
    if z_inds == [-1]
        res = Vector{T}(undef, size(data, 1))
        for i in axes(data, 1)
            data_view = view(data, i, :)
            res[i] = sum(data_view .* jacobian)
        end
        return res / length(jacobian)
    else
        return data
    end
end

function flux_spectrum_ky!(res::AbstractArray{T},
                           A::AbstractArray{Complex{T}},
                           B::AbstractArray{Complex{T}},
                           ky_inds::AbstractVector{Int},
                           z_inds::AbstractVector{Int}
                          ) where {T}
    size(A) == size(B) || throw(DimensionMismatch("Incorrect dimensions between A ($(size(A)) and B ($(size(B)))"))
    nky = size(A, 2)
    #kx_inds = which_kx == [-1] ? collect(1:nkx2) : which_kx
    #z_inds = which_z == [-1] ? collect(1:nz) : which_z
    #res = Array{T}(undef, length(kx_inds), length(z_inds))
    #temp = similar(res)
    @inbounds for (z, z_ind) in enumerate(z_inds)
        @views for (i, ky_ind) in enumerate(ky_inds)
            res[i, z] = flux_spectrum_ky(ky_ind, A[:, :, z_ind], B[:, :, z_ind])
        end
    end
    repeated = findall(i -> i == 1 || i == nky, ky_inds)
    if !isempty(repeated)
        res_view = view(res, repeated, :)
        res_view[:, :] .*= 0.5
    end

    return nothing
end

@inline function flux_spectrum_ky(ky_index::Int,
                                  A::AbstractArray{Complex{T}},
                                  B::AbstractArray{Complex{T}},
                                 ) where {T}
    res = zero(Complex{T})
    @inbounds for i in axes(A, 1)
        res += conj(A[i, ky_index]) * B[i, ky_index]
    end
    return 2 * real(res)
    #return 2 * real(sum(conj.(view(A, :, ky_index)) .* view(B, :, ky_index)))
end

function get_fields_and_moments(flux_diag::FluxSpectra{T, S, F}) where {T, S, F}
    data_fields = Symbol[]
    for flux in flux_diag.flux_list
        if flux === :Γ_es
            push!(data_fields, [:phi, :dens]...)
        elseif flux === :Q_es
            push!(data_fields, [:phi, :dens, :T_par, :T_perp]...)
        elseif flux === :P_es
            push!(data_fields, [:phi, :u_par]...)
        elseif flux === :Γ_em
            push!(data_fields, [:A_par, :u_par]...)
        elseif flux === :Q_em
            push!(data_fields, [:A_par, :q_par, :q_perp]...)
        elseif flux === :P_em
            push!(data_fields, [:A_par, :dens, :T_par]...)
        end
    end
    return unique(data_fields)
end

function get_diag_name(flux_diag::FluxSpectra{T, S, F};) where {T, S, F}
    return "fluxspectra"
end


#This doesn't include u_ExB_x, B_x, A, or B
function save_plot_data(flux_diag::FluxSpectra{T, S, F},
                        spec_index::Int,
                        fid;
                       ) where {T, S, F}
    fid["kx"] = flux_diag.kx[2]
    fid["ky"] = flux_diag.ky[2]
    for f in eachindex(flux_diag.flux_list)
        flux = String(flux_diag.flux_list[f])
        fid["$(flux), kx"] = flux_diag.plot_data[:kx][:,1,f,spec_index]
        fid["$(flux), ky"] = flux_diag.plot_data[:ky][:,1,f,spec_index]
    end
    return nothing
end
