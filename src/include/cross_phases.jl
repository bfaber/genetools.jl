#Cross phase diagnostic returns array for each pair of fields such that
# plot_data[1,ky,s] = mean angle, plot_data[2:end,ky,s] = histogram of phases
# Assumes z is averaged...

function CrossPhases(diag::DiagControl{T},
                     var_list::Union{NTuple{N, Symbol},Vector{NTuple{N,Symbol}}} = (:phi,:dens),
                     which_z::Union{Vector{S}, Vector{I}} = [div(length(diag.coords.z), 2) + 1],
                    ) where {T, N, S <: AbstractFloat, I <: Integer}
    plot_vars = Vector{Symbol}(undef,length(var_list))
    if var_list isa Vector
        vars = deepcopy(var_list)
        for v in eachindex(var_list)
            plot_vars[v] = Symbol(var_list[v][1],:_x_,var_list[v][2])
        end
    else
        vars = [deepcopy(var_list)]
        plot_vars = tuple(Symbol(var_list[1],:_x_,var_list[2]))
    end
    
    n_spec = length(diag.active_species)
    nz = length(diag.coords.z)
    z_tuple = if which_z == [-1]
        z = collect(range(start = 1, stop = length(diag.coords.z)))
        nz_plot = 1
        (true, z)
    else
        nz_plot = length(which_z)
        (false, which_z)
    end

    nx = length(diag.coords.x)
    nky = length(diag.coords.ky)
    ky_vec = diag.parameters[first(diag.run_IDs)][:general][:nonlinear] ? diag.coords.ky[2:end-1] : diag.coords.ky

    bin_size = 0.1
    nbins = Int(floor(2*π/bin_size))

    ax_args = map(i -> Expr(:kw, plot_vars[i], i), eachindex(plot_vars))
    ax = eval(Expr(:call, :(ComponentArrays.Axis), ax_args...))
    data = Array{Complex{T}, 3}(undef, nx, nky, nz)
    plot_data = eval(Expr(:call, :ComponentArray, map(i -> Expr(:kw, i, zeros(T, nbins + 2, length(ky_vec), n_spec)), plot_vars)...), ) #ComponentArray(mean = zeros(T, length(ky_vec), n_spec), histogram = zeros(T, nbins + 1, n_spec)))
    return CrossPhases{T, Tuple(diag.active_species), (:field, :mom)}(data, plot_data, vars, z_tuple, ky_vec)
end

function CrossPhases()
    return CrossPhases{Float64, (:i, :e), (:field, :mom)}(ComponentArray(phi = Vector{Float64}(),dens = Vector{Float64}()), (GeneTools.phi,GeneTools.dens), (true, false), (true, false))
end

function run_diag!(cross_phases_diag::CrossPhases{T, S, F},
                   diag::DiagControl{T},
                   timeslice_data::GeneTimesliceData{T, S, F},
                  ) where {T, S, F}
    cross_phases!(cross_phases_diag, diag, timeslice_data)
end

function cross_phases!(cross_phases_diag::CrossPhases{T, S, F},
                       diag::DiagControl{T},
                       timeslice_data::GeneTimesliceData{T, S, F}
                      ) where {T, S, F}
    #A = Array{Complex{T}}(undef, size(cross_phases_diag.data))
    #B = similar(A)
    for (j, s) in enumerate(species(cross_phases_diag))
        for (var1,var2) in cross_phases_diag.var_list
            data_view1 = if var1 in [:phi, :A_par, :B_par]
                filename = "field"
                view(timeslice_data.data[filename][var1], :, :, :)
            else
                filename = "mom_"*string(s)
                view(timeslice_data.data[filename][var1], :, :, :)
            end
            data_view2 = if var2 in [:phi, :A_par, :B_par]
                filename = "field"
                view(timeslice_data.data[filename][var2], :, :, :)
            else
                filename = "mom_"*string(s)
                view(timeslice_data.data[filename][var2], :, :, :)
            end
            
	    A = diag.parameters[first(diag.run_IDs)][:general][:nonlinear] ? (diag.ifft_plan_xz * data_view1)[:,2:end-1,:] : diag.ifft_plan_xz * data_view1
	    B = diag.parameters[first(diag.run_IDs)][:general][:nonlinear] ? (diag.ifft_plan_xz * data_view2)[:,2:end-1,:] : diag.ifft_plan_xz * data_view2
            add_plot_data!(cross_phases_diag, Symbol(var1,:_x_,var2), [:,:,j],
                           cross_phases!(A,B))
            #ploting and data-saving call here
        end
    end
end

function get_fields_and_moments(cross_phases_diag::CrossPhases{T, S, F}) where {T, S, F}
    data_fields = Symbol[]
    
    for phase in cross_phases_diag.var_list
        push!(data_fields,[phase[1], phase[2]]...)
    end
    return unique(data_fields)
end

#Uses all x,ky,z points in histogram. Not field line averages/integrated

function cross_phases!(A::AbstractArray{Complex{T}},
                       B::AbstractArray{Complex{T}}) where{T}
    nx,nky,nz = size(A)
    bin_size = 0.1
    nbins = Int(floor(2*π/bin_size))
    hist = zeros(T,nbins+1,nky)
    sumhist = similar(hist)
    data = A./B
    phase_data = atan.(imag.(data),real.(data))
    #phase_data = reshape(phase_data,(nx*nz,nky))
    for k in 1:nky
	hist[:,k] .= T.(fit(Histogram, reshape(phase_data[:,k,:],nx*nz), -π:2*π/(nbins+1):π).weights)
    end
    hist ./= nx*nz
    sumhist = hist
    winkind = -π .+ [0:1:nbins;]*2*π ./ nbins
    avsum = zeros(T,nky)
    avang = winkind[1]

    for j in eachindex(winkind)
        avsum .+= sumhist[j,:] * min(abs(winkind[1] - winkind[j]), 2*π - abs(winkind[1] - winkind[j]))
    end

    for i in eachindex(winkind)
	sum = zeros(T,nky)
        for j in eachindex(winkind)
            sum .+= sumhist[j,:] * min(abs(winkind[i] - winkind[j]), 2*π - abs(winkind[i] - winkind[j]))
        end
        if avsum > sum
            avsum = sum
            avang = winkind[i]
        end
    end
    return vcat(mean(avang),sumhist)#ComponentArray(mean = mean(avang), histogram = sumhist)
end

#Data saving routines
function get_diag_name(cross_phases_diag::CrossPhases{T, S, F};) where {T, S, F}
    return "cross_phase"
end

function save_plot_data(cross_phases_diag::CrossPhases{T, S, F},
                        spec_index::Int,
                        fid;
                       ) where {T, S, F}
    var_list = keys(cross_phases_diag.plot_data)
    nbins = size(cross_phases_diag.plot_data[var_list[1]])[1] - 2 
    fid["phases"] = -π .+ [0:1:nbins;]*2*π ./ nbins
    fid["ky"] = cross_phases_diag.ky_vec 
    for var in keys(cross_phases_diag.plot_data)
        string_var = String(var)
        create_group(fid, string_var)
	fid[string_var]["mean"] = length(cross_phases_diag.ky_vec) == 1 ? [cross_phases_diag.plot_data[var][1,:,spec_index]] : cross_phases_diag.plot_data[var][1,:,spec_index]
        fid[string_var]["histogram"] = cross_phases_diag.plot_data[var][2:end,:,spec_index]
    end
    return nothing
end

function save_plot_data(cross_phases_diag::CrossPhases{T, S, F},
                        spec_index::Int,
                        fid,
                        run_ID::String;
                       ) where {T, S, F}
    create_group(fid, run_ID)
    nbins = size(cross_phases_diag.plot_data)[1] - 2
    fid["$run_ID/phases"] = -π .+ [0:1:nbins;]*2*π ./ nbins
    fid["$run_ID/ky"] = cross_phases_diag.ky_vec
    for var in keys(cross_phases_diag.plot_data)
        string_var = String(var)
        create_group(fid, "$run_ID/$string_var")
        fid["$run_ID/$string_var"]["mean"] = [cross_phases_diag.plot_data[var][1,:,spec_index]]
        fid["$run_ID/$string_var"]["histogram"] = cross_phases_diag.plot_data[var][2:end,:,spec_index]
    end
    return nothing
end
